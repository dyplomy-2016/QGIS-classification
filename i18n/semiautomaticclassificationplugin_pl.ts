<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl_PL">
<context>
    <name>Dock</name>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="20"/>
        <source>SCP: ROI creation</source>
        <translatorcomment>ROI-Obszar treningowy</translatorcomment>
        <translation type="unfinished">SCP: Tworzenie OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="39"/>
        <source> Training shapefile</source>
        <translation type="unfinished">Treningowy shapefile</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="50"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Refresh list&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Odśwież listę&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message utf8="true">
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="53"/>
        <source>↺</source>
        <translation type="unfinished">↺</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="60"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create a new training shapefile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stwórz nowy treningowy shapefile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="63"/>
        <source>New shp</source>
        <translation type="unfinished">Nowy shp</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="70"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select a layer&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz warstwę&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="91"/>
        <source> ROI list</source>
        <translatorcomment>ROI-Obszar treningowy</translatorcomment>
        <translation type="unfinished">Lista OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="902"/>
        <source>MC ID</source>
        <translatorcomment>Macroclass ID</translatorcomment>
        <translation type="unfinished">MC ID</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="1032"/>
        <source>MC Info</source>
        <translatorcomment>Macroclass Information</translatorcomment>
        <translation type="unfinished">MC Info</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="1131"/>
        <source>C ID</source>
        <translatorcomment>Class ID</translatorcomment>
        <translation type="unfinished">C ID</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="1019"/>
        <source>C Info</source>
        <translatorcomment>Class Information</translatorcomment>
        <translation type="unfinished">C Info</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="159"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add highlihted ROIs to spectral signature plot&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>OT-obszary trningowe</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dodaj wybrane OT do wykresu charakterystyk spektralnych&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="186"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="173"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add highlighted ROIs to signature list&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>OT-obszary treningowe</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dodaj wybrane OT do listy sygnatur&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="176"/>
        <source>Add to signature</source>
        <translation type="unfinished">Dodaj do sygnatury</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="183"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show scatter plot&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pokaż wykres punktowy&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="197"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Delete highlighted ROIs&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>OT-Obszary treningowe</translatorcomment>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Usuń wybrane OT&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="200"/>
        <source>Delete</source>
        <translation type="unfinished">Usuń</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="242"/>
        <source> ROI parameters</source>
        <translatorcomment>OT-obszar treningowy</translatorcomment>
        <translation type="unfinished">Parametry OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="268"/>
        <source>Max ROI width   </source>
        <translatorcomment>Maksymalna szerokość obszaru treningowego</translatorcomment>
        <translation type="unfinished">Maks. szer. OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="290"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Side of a square which inscribes the ROI, defining the maximum width thereof (in pixel unit)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Bok kwadratu wpisanego w OT , wyznaczający jego maksymalną szerokość (w  pikselach)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="321"/>
        <source>Min ROI size      </source>
        <translatorcomment>OT- obszar treningowy</translatorcomment>
        <translation type="unfinished">Min. wielkość OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="340"/>
        <source>Range radius</source>
        <translation type="unfinished">Promień zasięgu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="353"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Minimum area of ROI (in pixel unit)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>OT-obszar treningowy</translatorcomment>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Minimalna powierzchnia OT (px)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="378"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Radius in the multispectral space (in radiometry unit)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Promień w przestrzeni wielospektralnej (w jednostce radiometrii)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="413"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Band number&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Numer kanału&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="432"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Calculate ROI only on one band&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wyznacz OT tylko na jednym kanale&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="435"/>
        <source>Rapid ROI on band</source>
        <translatorcomment>OT-obszar treningowy</translatorcomment>
        <translation type="unfinished">Szybki OT na kanale</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="444"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically refresh the temporary ROI, as the parameters change&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>OT-obszar treningowy</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatyczne odświeżanie tymczasowego OT przy zmianie parametrów&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="447"/>
        <source>Automatic refresh ROI</source>
        <translatorcomment>OT-obszar treningowy</translatorcomment>
        <translation type="unfinished">Automatyczne odświeżanie OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="457"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically calculate signature plot&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatycznie wyznacz wykres sygnatury&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="460"/>
        <source>Automatic  plot</source>
        <translation type="unfinished">Automatyczny wykres</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="494"/>
        <source> ROI creation</source>
        <translatorcomment>OT- obszar treningowy</translatorcomment>
        <translation type="unfinished">Tworzenie OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="543"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Redo the ROI at the same point&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Przywróć OT w tym samym punkcie&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message utf8="true">
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="546"/>
        <source>Redo ↺</source>
        <translation type="unfinished">Przywróć ↺</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="589"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Create a ROI polygon&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>OT-onszar treningowy</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Utwórz poligon OT&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="635"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Activate ROI pointer&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>OT-obszar treningowy</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Aktywuj wskaźnik OT&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="641"/>
        <source>+</source>
        <translation type="unfinished">+</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="669"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select a vegetation index&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz indeks roślinności&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="673"/>
        <source>NDVI</source>
        <translatorcomment>Znormalizowany różnicowy wskaźnik wegetacji</translatorcomment>
        <translation type="unfinished">NDVI</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="678"/>
        <source>EVI</source>
        <translatorcomment>Enhanced Vegetation Index </translatorcomment>
        <translation type="unfinished">EVI</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="699"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Display a vegetation index value with the cursor&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wyświetlaj pod kursorem indeks roślinności &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="702"/>
        <source>Display cursor for</source>
        <translation type="unfinished">Wyświetl kursorem</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="735"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Multiple ROI creation&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>OT-obszar treningowy</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Tworzenie wielu OT&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="766"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show/hide the temporary ROI&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>OT- obszar treningowy</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pokaż/ukryj tymczasowy OT&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="769"/>
        <source>Show</source>
        <translation type="unfinished">Pokaż</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="805"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Zoom to temporary ROI&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>OT-obszar treningowy</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Przybliż do tymczasowego OT&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="854"/>
        <source> ROI Signature definition</source>
        <translatorcomment>OT- obszar treningowy</translatorcomment>
        <translation type="unfinished">Określenie sygnatury OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="920"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Save the last ROI to training shapefile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>OT- obszar treningowy</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zapisz ostatni OT w treningowym OT &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="923"/>
        <source>Save ROI</source>
        <translatorcomment>OT- obszar treningowy</translatorcomment>
        <translation type="unfinished">Zapisz OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="944"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The macroclass ID of the ROI signature&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>OT-obszar treningowy</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Identyfikator makroklas sygnatury OT&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="975"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The class ID of the ROI signature&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>OT- obszar treningowy</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Kategoria identyfikatora sygnatury OT&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="1000"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The class name of the ROI signature&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>OT- obszar treningowy</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nazwa kategorii sygnatury OT&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="1003"/>
        <source>Class_1</source>
        <translation type="unfinished">Kategoria_1</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="1045"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The macroclass name of the ROI signature&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>OT- obszar treningowy</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nazwa makroklasy sygnatury OT&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="1048"/>
        <source>Macroclass_1</source>
        <translation type="unfinished">Makroklasa_1</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="1075"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Delete the last saved ROI&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>OT- obszar treningowy</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Usuń ostatnio zapisany OT&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message utf8="true">
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="1078"/>
        <source>↶ Undo</source>
        <translation type="unfinished">↶ Cofnij</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="1098"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add ROI spectral signature to signature list&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dodaj charakterystykę spektralną OT do listy sygnatur&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock.ui" line="1101"/>
        <source>Add sig. list</source>
        <translation type="unfinished">Dodaj listę sygnatur</translation>
    </message>
</context>
<context>
    <name>DockClass</name>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="20"/>
        <source>SCP: Classification</source>
        <translation type="unfinished">SCP: Klasyfikacja</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="45"/>
        <source> Signature list file</source>
        <translation type="unfinished">Plik z listą sygnatur</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="59"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open a signature list file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Otwórz plik z listą sygnatur&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="62"/>
        <source>Open</source>
        <translation type="unfinished">Otwórz</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="72"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Signature file path&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ścieżka do pliku z listą sygnatur&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="79"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Save a signature list&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zapisz listę sygnatur&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="82"/>
        <source>Save</source>
        <translation type="unfinished">Zapisz</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="89"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset signature list path&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>Zresetuj, wyzeruj</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zresetuj ścieżkę do listy sygnatur&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="954"/>
        <source>Reset</source>
        <translation type="unfinished">Reset</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="119"/>
        <source> Signature list</source>
        <translation type="unfinished">Lista sygnatur</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="155"/>
        <source>S</source>
        <translatorcomment>checkbox field</translatorcomment>
        <translation type="unfinished">S</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="160"/>
        <source>MC ID</source>
        <translatorcomment>Macroclass ID</translatorcomment>
        <translation type="unfinished">MC ID</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="165"/>
        <source>MC Info</source>
        <translatorcomment>signature Macroclass Information</translatorcomment>
        <translation type="unfinished">MC Info</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="170"/>
        <source>C ID</source>
        <translatorcomment>signature Class ID</translatorcomment>
        <translation type="unfinished">C ID</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="175"/>
        <source>C Info</source>
        <translatorcomment>signature Class Information</translatorcomment>
        <translation type="unfinished">C Info</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="180"/>
        <source>Color</source>
        <translation type="unfinished">Kolor</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="218"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Export highlighted signatures to CSV spectral library&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>CSV- rozszerzenie pliku/format pliku</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Eksportuj wybrane sygnatury do biblioteki spektralnej CSV&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="221"/>
        <source>Export to CSV</source>
        <translation type="unfinished">Eksportuj do CSV</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="232"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Delete highlighted signatures&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Usuń wybrane sygnatury&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="235"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="246"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Merge highlighted spectral signatures obtaining the average signature&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Połącz wybrane charakterystyki spektralne otrzymując średnią sygnaturę&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="311"/>
        <source>Plot</source>
        <translation type="unfinished">Wykres</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="260"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Import a spectral library&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Importuj blibliotekę spektralną&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="297"/>
        <source>Import library</source>
        <translation type="unfinished">Importuj bibliotekę</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="274"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Import and add a signature list file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Importuj i dodaj plik z listą sygnatur&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="277"/>
        <source>Import</source>
        <translation type="unfinished">Importuj</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="284"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Export the signature list to file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Eksportuj listę sygnatur do pliku&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="287"/>
        <source>Export</source>
        <translation type="unfinished">Eksportuj</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="294"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Import a USGS Spectral Library (requires internet connection)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>US Geological Survey</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Importuj Bibliotekę Spektralną USGS (wymaga połączenia z internetem)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="308"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add highlighted signatures to spectral signature plot&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dodaj wybrane sygnatury do wykresu charakterystyk spektralnych&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="355"/>
        <source> Classification algorithm</source>
        <translation type="unfinished">Algorytm klasyfikacji</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="384"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select a classification algorithm&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz algorytm klasyfikacji&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="388"/>
        <source>Minimum Distance</source>
        <translation type="unfinished">Minimalna odległość</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="393"/>
        <source>Maximum Likelihood</source>
        <translation type="unfinished">Maksymalne prawdopodobieństwo</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="398"/>
        <source>Spectral Angle Mapping</source>
        <translatorcomment>algorytm SAM</translatorcomment>
        <translation type="unfinished">Spectral Angle Mapping</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="406"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open tab Algorithm  weight&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Otwórz zakładkę Waga algorytmu&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="585"/>
        <source>W</source>
        <translation type="unfinished">W</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="432"/>
        <source>Select classification algorithm</source>
        <translation type="unfinished">Wybierz algorytm klasyfikacji</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="439"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use the ID of macroclasses for the classification&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Do klasyfikacji wybierz ID makroklas&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="442"/>
        <source>Use Macroclass ID</source>
        <translation type="unfinished">Użyj ID Makroklas</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="461"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set a classification threshold for all signatures&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ustaw próg klasyfikacji dla wszystkich sygnatur&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="477"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open tab Signature threshold&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Otwórz zakładkę Próg sygnatur&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="509"/>
        <source>Threshold</source>
        <translation type="unfinished">Próg</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="549"/>
        <source> Classification preview</source>
        <translation type="unfinished">Podgląd klasyfikacji</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="566"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show / hide preview&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pokaż/ukryj podgląd&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="569"/>
        <source>Show</source>
        <translation type="unfinished">Pokaż</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="582"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zoom to preview&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Przybliż do podglądu&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="608"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the preview size (in pixel unit)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ustaw rozmiar podglądu (w pikselach)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="645"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Redo the classification preview at the same point&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Przywróć podgląd klasyfikacji w tym samym punkcie&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message utf8="true">
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="648"/>
        <source>Redo ↺</source>
        <translation type="unfinished">Przywróc ↺</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="661"/>
        <source>Size</source>
        <translation type="unfinished">Rozmiar</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="688"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Activate Preview pointer&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Aktywuj Podgląd wskaźnika&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="691"/>
        <source>+</source>
        <translation type="unfinished">+</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="717"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set preview transparency&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ustaw przezroczystość podglądu&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="739"/>
        <source>Transparency</source>
        <translation type="unfinished">Przezroczystość</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="782"/>
        <source> Classification style</source>
        <translation type="unfinished">Styl klasyfikacji</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="802"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Qml file path&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ścieżka pliku qml&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="809"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select a qml file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz plik qml&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="812"/>
        <source>Select qml</source>
        <translation type="unfinished">Wybierz qml</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="831"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset qml style to default&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zresetuj styl qml do podstawowego&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="874"/>
        <source> Classification output</source>
        <translation>Wynik klasyfikacji</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="891"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Calculate a classification report&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;/p&gt;Twórz raport klasyfikacji&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="894"/>
        <source>Classification report</source>
        <translation type="unfinished">Raport klasyfikacji</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="901"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create a classification shapefile after the classification process&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Po procesie klasyfikacji stwórz plik shapefile z klasyfikacją &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="904"/>
        <source>Create vector</source>
        <translation type="unfinished">Stwórz wektor</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="913"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select an optional mask shapefile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz dodatkową maskę shapefile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="916"/>
        <source>Apply mask</source>
        <translation type="unfinished">Zastosuj maskę</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="926"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Path of the optional mask shapefile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ścieżka dodatkowej maski shapefile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="951"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset mask path&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zresetuj ścieżkę maski&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="976"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Perform the classification and save it as .tif file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dokonaj klasyfikacji i zapisz jako plik .tif&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_dock_class.ui" line="979"/>
        <source>Perform classification</source>
        <translation type="unfinished">Dokonaj klasyfikacji</translation>
    </message>
</context>
<context>
    <name>ScatterPlot</name>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_scatter_plot.ui" line="20"/>
        <source>SCP: Scatter Plot</source>
        <translation type="unfinished">SCP: Wykres punktowy</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_scatter_plot.ui" line="48"/>
        <source> ROI list</source>
        <translatorcomment>Obszary treningowe</translatorcomment>
        <translation type="unfinished">Lista OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_scatter_plot.ui" line="87"/>
        <source>S</source>
        <translation type="unfinished">S</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_scatter_plot.ui" line="92"/>
        <source>MC ID</source>
        <translation type="unfinished">MC ID</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_scatter_plot.ui" line="97"/>
        <source>MC Info</source>
        <translation type="unfinished">MC Info</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_scatter_plot.ui" line="102"/>
        <source>C ID</source>
        <translation type="unfinished">C ID</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_scatter_plot.ui" line="107"/>
        <source>C Info</source>
        <translation type="unfinished">C Info</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_scatter_plot.ui" line="112"/>
        <source>Color</source>
        <translation type="unfinished">Kolor</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_scatter_plot.ui" line="122"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Calculate scatter plot&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wyznacz wykres punktowy&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_scatter_plot.ui" line="125"/>
        <source>Calculate scatter plot</source>
        <translation type="unfinished">Wyznacz wykres punktowy</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_scatter_plot.ui" line="146"/>
        <source>Band Y</source>
        <translation type="unfinished">Kanał Y</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_scatter_plot.ui" line="165"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Band Y&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Kanał Y&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_scatter_plot.ui" line="200"/>
        <source>Band X</source>
        <translation type="unfinished">Kanał X</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_scatter_plot.ui" line="219"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Band X&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Kanał X&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>SemiAutomaticClassificationPlugin</name>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="425"/>
        <source> Input image</source>
        <translation type="unfinished">Obraz wejściowy</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="445"/>
        <source> RGB=</source>
        <translation type="unfinished"> RGB=</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="462"/>
        <source> Show</source>
        <translation type="unfinished">Pokaż</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="8073"/>
        <source>Semi-Automatic Classification Plugin</source>
        <translation type="unfinished">Semi-Automatic Classification Plugin</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="67"/>
        <source>Tools</source>
        <translation type="unfinished">Narzędzia</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="81"/>
        <source>Multiple ROI creation</source>
        <translatorcomment>Obszary treningowe</translatorcomment>
        <translation type="unfinished">Tworzenie wielu OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4044"/>
        <source>X</source>
        <translation type="unfinished">X</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4028"/>
        <source>Y</source>
        <translation type="unfinished">Y</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5894"/>
        <source>MC ID</source>
        <translation type="unfinished">MC ID</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="981"/>
        <source>MC Info</source>
        <translation type="unfinished">MC Info</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5899"/>
        <source>C ID</source>
        <translation type="unfinished">C ID</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="991"/>
        <source>C Info</source>
        <translation type="unfinished">C Info</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="132"/>
        <source>Min Size</source>
        <translation type="unfinished">Min rozmiar</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="137"/>
        <source>Max width</source>
        <translation type="unfinished">Maks szerokość</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="142"/>
        <source>Range radius</source>
        <translation type="unfinished">Promień zasięgu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="147"/>
        <source>Rapid ROI band</source>
        <translatorcomment>Obszar treningowy</translatorcomment>
        <translation type="unfinished">Szybki kanał OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="165"/>
        <source> Point coordinates and ROI definition</source>
        <translatorcomment>Obszar treningowy</translatorcomment>
        <translation type="unfinished">Współrzędne punktu i określenie OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="187"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add a row for a new point&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dodaj wiersz dla nowego punktu&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="190"/>
        <source>Add point</source>
        <translation>Dodaj punkt</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="197"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove highlighted points&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Usuń wybrane punkty&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="200"/>
        <source>Remove 
 highlighted points</source>
        <translation type="unfinished">Usuń 
 wybrane punkty</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="208"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Import point list from text file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Importuj liste punktów z pliku tekstowego&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="211"/>
        <source>Import point list</source>
        <translation type="unfinished">Importuj listę punktów</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="226"/>
        <source>Number of random points</source>
        <translation type="unfinished">Liczba punktów</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="236"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create random points inside each cell of a grid with this size&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stwórz losowe punkty wewnątrz każdej komórki siatki z tej wielkości&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="239"/>
        <source>inside a grid of cell size</source>
        <translation type="unfinished">Wewnątrz siatki o wielkości komórki</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="258"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Size of a grid cell within points are created randomly&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Wielkość komórki siatki wewnątrz punktów jest tworzona losowo&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="289"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Number of points created randomly&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Liczba punktów tworzonych losowo&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1243"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Minimum distance between points&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Minimalna odległość pomiędzy punktami&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="339"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Minimum distance between points&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Minimalna odległość pomiędzy punktami&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="342"/>
        <source>minimum point distance</source>
        <translation type="unfinished">minimalna odległość punktów</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="349"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create random points (coordinate reference system must be projected)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stwórz losowe punkty (układ współrzędnych odniesienia musi być rzutowany)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="352"/>
        <source>Create random points</source>
        <translation type="unfinished">Stwórz losowe punkty</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="361"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Export point list to text file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Eksportuj listę punktów do pliku tekstowego&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="364"/>
        <source>Export point list</source>
        <translation type="unfinished">Eksportuj listę punktów</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="388"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add ROI spectral signatures to signature list&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>obszary treningowe</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dodaj sygnatury OT do listy cech rozpoznawczych&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="391"/>
        <source>Add sig. list</source>
        <translation>Dodaj sygnatury</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="401"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create and save ROIs to shapefile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stwórz i zapisz OT do pliku  shapefile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="404"/>
        <source>Create and save ROIs</source>
        <translatorcomment>obszary treningowe</translatorcomment>
        <translation type="unfinished">Stwórz i zapisz OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="420"/>
        <source>USGS Spectral Library</source>
        <translatorcomment>United States Geological Survey</translatorcomment>
        <translation type="unfinished">Biblioteka Spektralna USGS</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="438"/>
        <source>Select a library</source>
        <translation type="unfinished">Wybierz bibliotekę</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="445"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select a library&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz bibliotekę&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="462"/>
        <source>Select a chapter</source>
        <translation type="unfinished">Wybierz rozdział</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="475"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Droid Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Droid Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="495"/>
        <source>Library Description (requires internet connection)</source>
        <translation type="unfinished">Opis Biblioteki (wymaga połączenia z internetem)</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="511"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;USGS Spectral Library downloaded from &lt;a href=&quot;http://speclab.cr.usgs.gov/spectral-lib.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://speclab.cr.usgs.gov/spectral-lib.html&lt;/span&gt;&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Reference&lt;/span&gt;: R. N. Clark, G. A. Swayze, R. Wise, K. E. Livo, T. M. Hoefen, R. F. Kokaly, and S. J. Sutley, 2007, USGS Digital Spectral Library splib06a, U.S. Geological Survey, Data Series 231.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biblioteka Spektralna USGS pobrana z &lt;a href=&quot;http://speclab.cr.usgs.gov/spectral-lib.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://speclab.cr.usgs.gov/spectral-lib.html&lt;/span&gt;&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Reference&lt;/span&gt;: R. N. Clark, G. A. Swayze, R. Wise, K. E. Livo, T. M. Hoefen, R. F. Kokaly, and S. J. Sutley, 2007, USGS Digital Spectral Library splib06a, U.S. Geological Survey, Data Series 231.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="524"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select a chapter&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz rozdział&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="546"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add signature to list (requires internet connection)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dodaj listę sygnatur (wymaga połączenia z internetem)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="549"/>
        <source>Add to signature list</source>
        <translation type="unfinished">Dodaj do listy sygnatur</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="565"/>
        <source>Algorithm band weight</source>
        <translation type="unfinished">Waga algorytmu kanału</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="573"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Band weight&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Waga kanału&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="601"/>
        <source>Band number</source>
        <translation>Numer kanału</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6678"/>
        <source>Band name</source>
        <translation type="unfinished">Nazwa kanału</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="611"/>
        <source>Weight</source>
        <translation type="unfinished">Waga</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="637"/>
        <source>Band weight</source>
        <translation type="unfinished">Waga kanału</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="653"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set weight value for highlighted bands&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ustaw wagi dla wybranych kanałów&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="656"/>
        <source>Set weight value</source>
        <translation type="unfinished">Ustaw wartość wagi</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="893"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set a value&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ustaw wartość&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="693"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset weights&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zresetuj wagi&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="696"/>
        <source>Reset weights</source>
        <translation type="unfinished">Zresetuj wagi</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="718"/>
        <source>Weight value</source>
        <translation type="unfinished">Wartość wagi</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="776"/>
        <source>Signature threshold</source>
        <translation type="unfinished">Próg sygnatur</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="792"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set threshold value for highlighted signatures&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ustaw wartość progu dla wybranych sygnatur&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="795"/>
        <source>Set threshold value</source>
        <translation type="unfinished">Ustaw wartość progu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="835"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset thresholds&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zresetuj progi&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="838"/>
        <source>Reset  thresholds</source>
        <translation type="unfinished">Zresetuj progi</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="858"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set automatically a threshold for all the highlighted signatures based on the standard deviation thereof&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatycznie ustaw próg dla wszystkich wybranych sygnatur w oparciu o ich odchylenia standardowe&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="861"/>
        <source>Automatic  thresholds</source>
        <translation type="unfinished">Automatyczne progi</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="883"/>
        <source>Multiplicative value</source>
        <translation>Mnożnik</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="926"/>
        <source>Threshold value</source>
        <translation type="unfinished">Wartość progu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="996"/>
        <source>Threshold</source>
        <translation type="unfinished">Próg</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1011"/>
        <source>Download Landsat</source>
        <translation type="unfinished">Pobierz Landsat</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2787"/>
        <source>Y (Lat)</source>
        <translatorcomment>latitude-szerokość</translatorcomment>
        <translation type="unfinished">Y (Szer.)</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2803"/>
        <source>X (Lon)</source>
        <translatorcomment>longitude-długość</translatorcomment>
        <translation type="unfinished">X (Dł.)</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4074"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Lower right Y&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Prawy dolny Y&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3980"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Lower right X&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Prawy dolny  X&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3947"/>
        <source>LR</source>
        <translation type="unfinished">PD</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4094"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Click the map for the lower right coordinates&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Kliknij mapę aby uzyskać współrzędne prawe dolne&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6134"/>
        <source>+</source>
        <translation type="unfinished">+</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2389"/>
        <source> Area coordinates</source>
        <translation type="unfinished">Współrzędne obszaru</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1155"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Find Landsat images and add them to the Image list table&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Znajdź obrazy z satelity Landsat i dodaj je do Tabeli z listą obrazów&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2335"/>
        <source>Find images</source>
        <translation type="unfinished">Znajdź obrazy</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2415"/>
        <source> Search</source>
        <translation type="unfinished">Szukaj</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2439"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Image ID&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>Identyfikator</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;ID obrazu&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1221"/>
        <source>Max cloud cover (%)</source>
        <translation type="unfinished">Maks. pokrycie chmur (%)</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2457"/>
        <source>to</source>
        <translation type="unfinished">do</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2473"/>
        <source>Acqusition date from</source>
        <translation>Data od</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2537"/>
        <source>yyyy-MM-dd</source>
        <translation type="unfinished">rrrr-mm-dd</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2566"/>
        <source>Image ID</source>
        <translation type="unfinished">ID obrazu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1412"/>
        <source>7 ETM+</source>
        <translation type="unfinished">7 ETM+</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1422"/>
        <source>8 OLI</source>
        <translation type="unfinished">8 OLI</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1432"/>
        <source>4-5 TM</source>
        <translation type="unfinished">4-5 TM</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1448"/>
        <source> Satellites</source>
        <translation type="unfinished">Satelity</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4054"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Upper left X&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Lewy górny X&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4064"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Upper left Y&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Lewy górny Y&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3934"/>
        <source>UL</source>
        <translation type="unfinished">LG</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4084"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Click the map for the upper left coordinates&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Kliknij mapę aby uzyskać współrzędne lewe górne&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1572"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Update Landsat image database (requires internet connection)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zaktualizuj bazę danych obrazów satelity Landsat (wymaga połączenia z internetem)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1575"/>
        <source>Update database</source>
        <translation type="unfinished">Zaktualizuj bazę danych</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1600"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select a directory where Landsat databases are stored&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz katalog, w którym przechowywane są bazy danych satelity Landsat&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1603"/>
        <source>Select database directory</source>
        <translation>Wybierz bazę danych katalogu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1626"/>
        <source> Database</source>
        <translation type="unfinished">Baza danych</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1651"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Database directory&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Katalog bazy danych&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1658"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, download only the Landsat 8 database from Amazon&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Jeżeli zaznaczone, pobierz tylko bazę danych z satelity Landsat 8 ze strony Amazon&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1661"/>
        <source>only Landat 8</source>
        <translation type="unfinished">tylko Landsat 8</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1686"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset Landsat database directory&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zresetuj katalog bazy danych satelity Landsat&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1689"/>
        <source>Reset directory</source>
        <translation type="unfinished">Zresetuj katalog</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1727"/>
        <source>Landsat images</source>
        <translation type="unfinished">Obrazy Landsat</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2884"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Display preview of highlighted images in QGIS&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wyświetl podgląd wybranych obrazów w QGIS&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2887"/>
        <source>Display image preview</source>
        <translation type="unfinished">Wyświetl podgląd obrazu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2912"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove highlighted images from the table&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Usuń wybrane obrazy z tabeli&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2915"/>
        <source>Remove images from list</source>
        <translation type="unfinished">Usuń obrazy z listy</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2940"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Clear all the table&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wyczyść całą tabelę&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2943"/>
        <source>Clear table</source>
        <translation type="unfinished">Wyczyść tabelę</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2962"/>
        <source> Image list</source>
        <translation type="unfinished">Lista obrazów</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3046"/>
        <source>ImageID</source>
        <translation type="unfinished">ID obrazu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2996"/>
        <source>AcquisitionDate</source>
        <translation type="unfinished">Data pozyskania</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3001"/>
        <source>CloudCover</source>
        <translation type="unfinished">Pokrycie chmur</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3006"/>
        <source>Path</source>
        <translation type="unfinished">Ścieżka</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3011"/>
        <source>Row</source>
        <translation>Wiersz</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3016"/>
        <source>min_lat</source>
        <translation type="unfinished">min_szer</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3021"/>
        <source>min_lon</source>
        <translation type="unfinished">min_dł</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3026"/>
        <source>max_lat</source>
        <translation type="unfinished">maks_szer</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3031"/>
        <source>max_lon</source>
        <translation type="unfinished">maks_dł</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1907"/>
        <source>Service</source>
        <translation type="unfinished">Usługa</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3041"/>
        <source>Preview</source>
        <translation type="unfinished">Podgląd</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1934"/>
        <source>Download options</source>
        <translation type="unfinished">Pobierz opcje</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1942"/>
        <source>Band 6</source>
        <translation type="unfinished">Kanał 6</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1952"/>
        <source>Band 4</source>
        <translation type="unfinished">Kanał 4</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="1962"/>
        <source>Band 10</source>
        <translation type="unfinished">Kanał 10</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2005"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select all bands&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz wszystkie kanały&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2008"/>
        <source>Check/uncheck all bands</source>
        <translation type="unfinished">Zaznacz/odznacz wszystkie kanały</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2043"/>
        <source>Band 1</source>
        <translation type="unfinished">Kanał 1</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2053"/>
        <source>Band 3</source>
        <translation type="unfinished">Kanał 3</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2063"/>
        <source>Band 9</source>
        <translation type="unfinished">Kanał 9</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2073"/>
        <source>Band 7</source>
        <translation type="unfinished">Kanał 7</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2083"/>
        <source>Band QA</source>
        <translation type="unfinished">Kanał QA</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2093"/>
        <source>Band 2</source>
        <translation type="unfinished">Kanał 2</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2103"/>
        <source>Band 8 (Panchromatic)</source>
        <translation type="unfinished">Kanał 8 (Panchromatyczny)</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2113"/>
        <source>Band 11</source>
        <translation type="unfinished">Kanał 11</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2123"/>
        <source>Band 5</source>
        <translation type="unfinished">Kanał 5</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2143"/>
        <source> Landsat 8 bands</source>
        <translation type="unfinished">Kanały Landsat 8</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2186"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pre process Landsat images&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Przetwarzanie obrazów z satelity Landsat&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2189"/>
        <source>Pre process images</source>
        <translation type="unfinished">Przetwarzanie obrazów</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3113"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Download images in image list table&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pobierz obrazy do tabeli z listą obrazów&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3116"/>
        <source>Download images from list</source>
        <translation type="unfinished">Pobierz obrazy z listy</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3136"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Load images in QGIS after download&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Po pobraniu załaduj obrazy w QGIS&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3139"/>
        <source>Load bands in QGIS</source>
        <translation type="unfinished">Załaduj kanały w QGIS</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3064"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Export download links to a text file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Eksportuj pobrane łącza do pliku tekstowego&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3067"/>
        <source>Export links</source>
        <translation type="unfinished">Eksportuj łącza</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3162"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Download images from list only if the corresponding previews are loaded in QGIS&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pobierz obrazy z listy, tylko gdy odpowiednie podglądy są załadowane do QGIS&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3165"/>
        <source>only if preview in Layers</source>
        <translation type="unfinished">tylko, gdy podgląd w warstwach</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3090"/>
        <source> Download</source>
        <translation type="unfinished">Pobierz</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2291"/>
        <source>Download Sentinel</source>
        <translation type="unfinished">Pobierz z Sentinel</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2332"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Find Sentinel images and add them to the Image list table&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Znajdź obrazy z satelity Sentinel i dodaj je do Tabeli z listą obrazów&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2607"/>
        <source>Password</source>
        <translation type="unfinished">Hasło</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2630"/>
        <source>Login Sentinels https://scihub.esa.int/dhus/</source>
        <translation type="unfinished">Zaloguj się do Sentinel https://scihub.esa.int/dhus/</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2640"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, remember user name and password locally in QGIS&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Jeżeli zaznaczone, zapamiętaj nazwę użytkownika i hasło lokalnie w QGIS&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2643"/>
        <source>remember</source>
        <translation type="unfinished">zapamiętaj</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2653"/>
        <source>User</source>
        <translation type="unfinished">Użytkownik</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2660"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;User name&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nazwa użytkownika&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2667"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Password&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Hasło&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2856"/>
        <source>Sentinel images</source>
        <translation type="unfinished">Obrazy z Sentinel</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="2991"/>
        <source>ImageName</source>
        <translation type="unfinished">NazwaObrazu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3036"/>
        <source>Size</source>
        <translation type="unfinished">Rozmiar</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3186"/>
        <source>Pre processing</source>
        <translation type="unfinished">Przetwarzanie</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3202"/>
        <source>Landsat</source>
        <translation type="unfinished">Landsat</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3241"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select directory containing Landsat bands&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz katalog zawietający kanały satelity Landsat&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3244"/>
        <source>Select directory</source>
        <translation type="unfinished">Wybierz katalog</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3276"/>
        <source>Directory containing Landsat bands</source>
        <translation type="unfinished">Katalog zawierający kanały Landsat</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3302"/>
        <source> Landsat conversion to TOA reflectance and brightness temperature</source>
        <translation>Konwersja Landsat do współczynnika odbicia TOA i temperatury luminancyjnej</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3309"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enable/Disable calculation of temperature in Celsius from thermal band&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Włącz/wyłącz wyliczanie temperatury w st. Celsjusza z kanału termalnego&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3312"/>
        <source> Brightness temperature in Celsius</source>
        <translation>Temperatura luminancyjna w st. Celsjusza</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3325"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enable/Disable the DOS1 atmospheric correction (thermal band is not corrected)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Włącz/Wyłącz korekcję atmosferyczną DOS1 (kanał termalny nie jest korygowany)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3328"/>
        <source> Apply DOS1 atmospheric correction</source>
        <translation type="unfinished">Zastosuj korekcję atmosferyczną DOS1</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3343"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;No data value&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wartość &quot;brak danych&quot;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3369"/>
        <source>Use NoData value (image has black border)</source>
        <translation type="unfinished">Użyj wartości BrakDanych (obraz ma czarne obramowanie)</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3406"/>
        <source>Select MTL file (if not in Landsat directory)</source>
        <translation type="unfinished">Wybierz plik MTL (jeżeli nie jest w katalogu Landsat)</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3447"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select MTL file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz plik MTL&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3450"/>
        <source>Select a MTL file</source>
        <translation type="unfinished">Wybierz plik MTL</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3457"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Perform pan-sharpening (Brovey Transform)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zastosuj metodę wyostrzania (Transformacja Brovey&apos;a)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3460"/>
        <source>Perform pan-sharpening (Landsat 7 or 8)</source>
        <translation type="unfinished">Zastosuj metodę wyostrzania (Landsat 7 lub 8)</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3479"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create a virtual raster&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stwórz raster wirtualny&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3482"/>
        <source>Create Virtual Raster</source>
        <translation>Stwórz Raster Wirtualny</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3505"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Perform band conversion&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wykonaj konwersję kanału&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3508"/>
        <source>Perform conversion</source>
        <translation type="unfinished">Wykonaj konwersję</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3528"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create the Band set automatically&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatycznie Stwórz Zespół kanałów&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3531"/>
        <source>Create Band set</source>
        <translation type="unfinished">Stwórz zespół kanałów</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3567"/>
        <source>Satellite</source>
        <translation type="unfinished">Satelita</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3596"/>
        <source>Sun elevation</source>
        <translation type="unfinished">Wysokość słońca</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3606"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;DATE ACQUIRED&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;DATA POZYSKANIA&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3632"/>
        <source>Date (YYYY-MM-DD)</source>
        <translation type="unfinished">Data (RRRR-MM-DD)</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3661"/>
        <source>Earth sun distance</source>
        <translation>Odlegość Ziemia-Słońce</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3671"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;SUN ELEVATION&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;WYSOKOŚĆ SŁOŃCA&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3678"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Earth sun distance&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Odległość Ziemi do Słońca&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3685"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove highlighted bands&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Usuń wybrane kanały&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3688"/>
        <source>Remove band</source>
        <translation type="unfinished">Usuń kanał</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3711"/>
        <source>Metadata</source>
        <translation type="unfinished">Metadane</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3718"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Satellite (e.g. LANDSAT8)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Satelita (np. LANDSAT8)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3727"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Edit metadata&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Edytuj metadane&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3734"/>
        <source>Band</source>
        <translation type="unfinished">Kanał</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3739"/>
        <source>RADIANCE_MULT</source>
        <translation type="unfinished">RADIANCE_MULT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3744"/>
        <source>RADIANCE_ADD</source>
        <translation type="unfinished">RADIANCE_ADD</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3749"/>
        <source>REFLECTANCE_MULT</source>
        <translation type="unfinished">REFLECTANCE_MULT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3754"/>
        <source>REFLECTANCE_ADD</source>
        <translation type="unfinished">REFLECTANCE_ADD</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3759"/>
        <source>RADIANCE_MAXIMUM</source>
        <translation type="unfinished">RADIANCE_MAXIMUM</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3764"/>
        <source>REFLECTANCE_MAXIMUM</source>
        <translation type="unfinished">REFLECTANCE_MAXIMUM</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3769"/>
        <source>K1_CONSTANT</source>
        <translation type="unfinished">K1_CONSTANT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3774"/>
        <source>K2_CONSTANT</source>
        <translation type="unfinished">K2_CONSTANT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3779"/>
        <source>LMAX</source>
        <translation type="unfinished">LMAX</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3784"/>
        <source>LMIN</source>
        <translation type="unfinished">LMIN</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3789"/>
        <source>QCALMAX</source>
        <translation type="unfinished">QCALMAX</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3794"/>
        <source>QCALMIN</source>
        <translation type="unfinished">QCALMIN</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3809"/>
        <source>Clip multiple rasters</source>
        <translation type="unfinished">Złącz wiele rastrów</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6631"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select Rasters&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz Rastry&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6548"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Refresh list of rasters&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Odśwież listę z rastrami&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6551"/>
        <source>Refresh list</source>
        <translation type="unfinished">Odśwież listę</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6558"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select all rasters&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz wszystkie rastry&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6561"/>
        <source>Select all</source>
        <translation type="unfinished">Wybierz wszystko</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3897"/>
        <source>Select rasters to clip</source>
        <translation type="unfinished">Wybierz rastry do złączenia</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3917"/>
        <source> Raster list</source>
        <translation type="unfinished">Lista rastrów</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="3970"/>
        <source> Clip coordinates</source>
        <translation type="unfinished">Złącz współrzędne</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4134"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use shapefile boundaries for clipping rasters&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Użyj obramowania shapefile do złączenia rastrów&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4137"/>
        <source>Use shapefile for clipping</source>
        <translation type="unfinished">Użyj shapefile do złączenia</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4150"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select the shapefile for clipping&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz plik shapefile do złączenia&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4644"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Refresh list of shapefiles&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Odśwież listę plików shapefile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4210"/>
        <source>NoData value</source>
        <translation type="unfinished">Wartość BrakDanych</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6323"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;NoData value&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wartość BrakDanych&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4517"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Output name prefix&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Prefiks nazwy wynikowej&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4251"/>
        <source>clip</source>
        <translation type="unfinished">złącz</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4555"/>
        <source>Output name prefix</source>
        <translation>Prefiks nazwy wynikowej</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4310"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Clip selected rasters&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Złącz wybrane rastry&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4313"/>
        <source>Clip selected rasters</source>
        <translation type="unfinished">Złącz wybrane rastry</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4353"/>
        <source>Split raster bands</source>
        <translation>Rozdziel kanały rastra</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4367"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select the image to be split&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz obraz który ma zostać rozdzielony&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4390"/>
        <source>Raster input</source>
        <translation type="unfinished">Wejście rastra</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4450"/>
        <source>Select a multiband raster</source>
        <translation type="unfinished">Wybierz raster wielospektralny</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4464"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Split selected raster&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Rozdziel wybrany raster&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4467"/>
        <source>Split selected raster</source>
        <translation type="unfinished">Rozdziel wybrany raster</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4520"/>
        <source>split</source>
        <translation type="unfinished">rozdziel</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4579"/>
        <source>Post processing</source>
        <translation type="unfinished">Przetwarzanie końcowe</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4593"/>
        <source>Accuracy</source>
        <translation type="unfinished">Dokładność</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4619"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select the field of the classification code &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz pole kodu klasyfikacji&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4660"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select a reference shapefile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz plik shapefile odniesienia&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4720"/>
        <source>Select the classification to assess</source>
        <translation type="unfinished">Wybierz klasyfikację do oszacowania</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4746"/>
        <source> Error Matrix Input</source>
        <translation>Macierz błędów</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4772"/>
        <source>Select the reference shapefile or raster</source>
        <translation type="unfinished">Wybierz shapefile odniesienia lub raster</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4788"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select the classification to assess&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz klasyfikację do oszacowania&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4826"/>
        <source>Shapefile field</source>
        <translation>Pole shapefile</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4851"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Courier 10 Pitch&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Courier 10 Pitch&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4873"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Calculate the error matrix&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Oblicz macierz błędu&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4876"/>
        <source>Calculate error matrix</source>
        <translation>Oblicz macierz błędu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4892"/>
        <source>Land cover change</source>
        <translation>Zmiana pokrycia terenu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5699"/>
        <source> Classification Input</source>
        <translation type="unfinished">Wejście klasyfikacji</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4929"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select the reference classification raster&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz odniesienie klasyfikacji rastra&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="4983"/>
        <source>Select the new classification</source>
        <translation type="unfinished">Wybierz nową klasyfikację</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5018"/>
        <source>Select the reference classification</source>
        <translation>Wybierz klasyfikację referencyjną</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5062"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select a new raster to be compared with the reference raster&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz nowy raster w celu porównania z rastrem referencyjnym&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5375"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Calculate land cover change raster and statistics thereof&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wyznacz zmiany pokrycia terenowego rastra i ich statystyki&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5089"/>
        <source>Calculate land cover change</source>
        <translation type="unfinished">Wyznacz zmiany pokrycia terenowego</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5109"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;If enabled, pixels having the same values in both classifications will be reported; otherwise unchanged pixels are set to 0.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Jeżeli włączone, piksele z takimi samymi wartościami w oby klasyfikacjach będą zgłaszane; inaczej nie zmienione piksele sa ustawione na 0.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5112"/>
        <source>Report unchanged pixels</source>
        <translation type="unfinished">Zgłoś nie zmienione piksele</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5150"/>
        <source>Classification report</source>
        <translation type="unfinished">Raport klasyfikacji</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5676"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select the classification raster&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz raster klasyfikacji&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5660"/>
        <source>Select the classification</source>
        <translation type="unfinished">Wybierz klasyfikację</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5259"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, NoData value will be excluded from the report&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Jeżeli zaznaczone, wartość BrakDanych zostanie wyłączona z raportu&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6339"/>
        <source>Use NoData value</source>
        <translation type="unfinished">Użyj wartości BrakDanych</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5314"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Calculate classification report&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wyznacz raport klasyfikacji&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5317"/>
        <source>Calculate classification report</source>
        <translation type="unfinished">Wyznacz raport klasyfikacji</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5378"/>
        <source>Save report to file</source>
        <translation>Zapisz raport w pliku</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5392"/>
        <source>Classification to vector</source>
        <translation type="unfinished">Klasyfikacja do wektora</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5503"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use the codes from Signature list table for shapefile legend&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Użyj kody z Tabeli listy sygnatur dla legendy pliku shapefile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5506"/>
        <source>Use code from Signature list</source>
        <translation type="unfinished">Użyj kod z Listy sygnatur</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7713"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select the code field&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz pole kodu&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5569"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Convert classification to shapefile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Konwertuj klasyfikację do pliku shapefile&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5572"/>
        <source>Convert classification to vector</source>
        <translation type="unfinished">Konwertuj klasyfikację na wektor</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5599"/>
        <source>Reclassification</source>
        <translation type="unfinished">Ponowna klasyfikacja</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5710"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, the reclassification table is filled according to the &apos;Signature list&apos; when clicking &apos;Calculate unique values&apos;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Jeżelii zaznaczone, tabela ponownej klasyfikacji jest wypełniona zgodnie z &quot;Listą sygnatur&quot; po kliknięciu &quot;Wyznacz unikalne wartości&quot; &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5713"/>
        <source>automatic C ID to MC ID values using codes from Signature list</source>
        <translation type="unfinished">automatycznie wartości C ID do MC ID używając kody z Listy sygnatur</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5723"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Calculate unique values from classification input and fill the reclassification table&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wyznacz niepowtarzalne wartości z wejścia klasyfikacji i wypełnij tabelę ponownej klasyfikacji&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5726"/>
        <source>Calculate unique values</source>
        <translation type="unfinished">Wyznacz niepowtarzalne wartości</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5750"/>
        <source>Old value</source>
        <translation type="unfinished">Stare wartości</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5755"/>
        <source>New value</source>
        <translation type="unfinished">Nowe wartości</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5765"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add a row for a new value&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dodaj wiersz dla nowej wartości&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5768"/>
        <source>Add value</source>
        <translation type="unfinished">Dodaj wartość</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5775"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Delete the rows of selected values&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Usuń wiersze z wybranymi wartościami&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5778"/>
        <source>Remove 
 highlighted values</source>
        <translation type="unfinished">Usuń 
 wybrane wartości</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5807"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The variable name can be defined in the tab Settings&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zmienna nazwa może być zdefiniowana w zakładce Ustawienia&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5816"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Field Old value accepts expressions using the variable name &apos;raster&apos;, following Python operators e.g. (raster &amp;lt; 3) | (raster &amp;gt; 3)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pole Stara wartość akceptuje wyrażenia używające zmiennej nazwy &quot;raster&quot;, poprzedzający operatory Python&apos;a.np. (raster &amp;lt; 3) | (raster &amp;gt; 3)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5833"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reclassify the classification in a new raster&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reklasyfikuj wynik w nowym rastrze&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5836"/>
        <source>Reclassify</source>
        <translation>Reklasyfikuj</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5871"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use the colors from Signature list table&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Użyj kolorów z tabeli z listy Sygnatury&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5874"/>
        <source>Apply symbology from Signature list</source>
        <translation type="unfinished">Zastosuj symbolikę z listy Sygnatury</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5890"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select the code field for symbology&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz pole kody dla symboliki&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5920"/>
        <source>Band calc</source>
        <translation type="unfinished">Wyznacz kanał</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5941"/>
        <source>Raster bands</source>
        <translation>Kanały rastra</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="5983"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Band list&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Lista kanałów&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6011"/>
        <source>Variable</source>
        <translation type="unfinished">Zmienna</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6037"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Inverse cosine&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Arcus cosinus&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6040"/>
        <source>acos</source>
        <translation type="unfinished">arc cos</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6047"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sine&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sinus&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6050"/>
        <source>sin</source>
        <translation type="unfinished">sin</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6057"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Inverse sine&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Arcus sinus&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6060"/>
        <source>asin</source>
        <translation type="unfinished">arc sin</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6067"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cosine&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cosinus&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6070"/>
        <source>cos</source>
        <translation type="unfinished">cos</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6077"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Tangent&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Tangens&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6080"/>
        <source>tan</source>
        <translation type="unfinished">tan</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6087"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Inverse tangent&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Arcus tangens&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6090"/>
        <source>atan</source>
        <translation type="unfinished">arc tan</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6101"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Multiplication&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Mnożenie&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6104"/>
        <source>*</source>
        <translation type="unfinished">*</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6111"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Power&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Potęga&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6114"/>
        <source>^</source>
        <translation type="unfinished">^</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6121"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Minus&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Minus&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6124"/>
        <source>-</source>
        <translation type="unfinished">-</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6131"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Plus&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Plus&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6141"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Division&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dzielenie&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6144"/>
        <source>/</source>
        <translation type="unfinished">/</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6151"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Close parenthesis&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zamknij nawias&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6154"/>
        <source>)</source>
        <translation type="unfinished">)</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6161"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Square root&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pierwiastek kwadratowy&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message utf8="true">
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6164"/>
        <source>√</source>
        <translation type="unfinished">√</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6171"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open parenthesis&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Otwórz nawias&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6174"/>
        <source>(</source>
        <translation type="unfinished">(</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6198"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Natural logarithm&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Logarytm naturalny&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6201"/>
        <source>ln</source>
        <translation type="unfinished">ln</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6208"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pi&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Liczba Pi&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message utf8="true">
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6211"/>
        <source>π</source>
        <translation type="unfinished">π</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6218"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Exponential&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Eksponent&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6221"/>
        <source>exp</source>
        <translation type="unfinished">exp</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6228"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Conditional where function&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Funkcja warunkowa&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6231"/>
        <source>np.where</source>
        <translation>np. where</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6250"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;For other functions see &lt;a href=&quot;http://docs.scipy.org/doc/numpy/reference/routines.math.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://docs.scipy.org/doc/numpy/reference/routines.math.html&lt;/span&gt;&lt;/a&gt; and insert the function in the expression with prefix &lt;span style=&quot; font-weight:600;&quot;&gt;np.&lt;/span&gt; (e.g. np.log10(raster1) + np.where(&amp;quot;raster1&amp;quot; &amp;gt; 1, 1, 0) )&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dla innych funkcji zobacz &lt;a href=&quot;http://docs.scipy.org/doc/numpy/reference/routines.math.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://docs.scipy.org/doc/numpy/reference/routines.math.html&lt;/span&gt;&lt;/a&gt;i wstaw funkcję w wyrażeniu z przedrostkiem&lt;span style=&quot; font-weight:600;&quot;&gt;np.&lt;/span&gt; (e.g. np.log10(raster1) + np.where(&amp;quot;raster1&amp;quot; &amp;gt; 1, 1, 0) )&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6296"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Calculate expression and create a new raster&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Oblicz wyrażenie i stwórz nowy raster&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6299"/>
        <source>Calculate</source>
        <translation type="unfinished">Oblicz</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6336"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, pixels equal to NoData value will be excluded from the output raster&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Jeżeli zaznaczone, piksele równe wartości BrakDanych zostaną wyłączone z rastra wyjściowego&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6346"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, the extent of raster ouput equals the extent of selected raster&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Jeżeli zaznaczone, rozszerzenie wyjścia rastra będzie równe rozszerzeniu rastra&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6349"/>
        <source>Same as</source>
        <translation type="unfinished">Takie jak</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6362"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select a raster&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz raster&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6369"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, the extent of raster ouput equals the intersection of input rasters&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Jeżeli zaznaczone, rozszerzenie wyjścia rastra będzie takie jak przecięcie wejścia rastra&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6372"/>
        <source>Intersection</source>
        <translation type="unfinished">Przecięcie</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6426"/>
        <source>Extent:</source>
        <translation>Zasięg:</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6446"/>
        <source>Output raster</source>
        <translation type="unfinished">Raster wyjściowy</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6457"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enter an expression (e.g. raster1 + raster2 )&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wpisz wyrażenie (np. raster1 + raster2 )&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6477"/>
        <source>Expression</source>
        <translation type="unfinished">Wyrażenie</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6603"/>
        <source> Band list</source>
        <translation type="unfinished">Lista kanałów</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6510"/>
        <source>Band set</source>
        <translation type="unfinished">Zestaw kanałów</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6520"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add selected rasters to band set&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dodaj zaznaczone rastry do Zestawu kanałów&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6523"/>
        <source>Add rasters to set</source>
        <translation type="unfinished">Dodaj rastry do zestawu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6583"/>
        <source>Select raster bands</source>
        <translation type="unfinished">Wybierz kanały rastra</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6650"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Band set&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zestaw kanałów&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6683"/>
        <source>Center wavelength</source>
        <translation>Centralna długość fali</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6688"/>
        <source>Multiplicative Factor</source>
        <translation>Mnożnik</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6693"/>
        <source>Additive Factor</source>
        <translation type="unfinished">Współczynnik addytywny</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6705"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove highlighted bands from band set&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Usuń wybrane kanały z Zestawu kanałów&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6708"/>
        <source>Remove 
 band</source>
        <translation type="unfinished">Usuń 
 kanał</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6716"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Clear the band set&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Oczyść Zestaw kanałów&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6719"/>
        <source>Clear all</source>
        <translation type="unfinished">Oczyść wszystko</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6728"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move highlihted band down&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Przenieś wybrane kanały niżej&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message utf8="true">
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6731"/>
        <source>↓</source>
        <translation type="unfinished">↓</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6738"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move highlihted band up&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Przenieś wybrane kanały wyżej&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message utf8="true">
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6741"/>
        <source>↑</source>
        <translation type="unfinished">↑</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6748"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sort bands by name (priority to ending number)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sortuj według nazw kanałów (priorytet dla numerów końcowych)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6751"/>
        <source>Sort by Name</source>
        <translation type="unfinished">Sortuj według Nazwy</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6775"/>
        <source>Control bands</source>
        <translation type="unfinished">Kanały kontrolne</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6793"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Import band set from file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Importuj zestaw kanałów z pliku&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6796"/>
        <source>Import</source>
        <translation type="unfinished">Importuj</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6803"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Export band set to file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Exportuj zestaw kanałów do pliku&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6806"/>
        <source>Export</source>
        <translation type="unfinished">Eksportuj</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6845"/>
        <source>Wavelength unit</source>
        <translation type="unfinished">Jednostka długości fali</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6855"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wavelength unit&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Jednostka długości fali&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6897"/>
        <source> Band set definition</source>
        <translation type="unfinished">Definicja zestawu kanałów</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6926"/>
        <source> Quick wavelength settings</source>
        <translation type="unfinished">Szybkie ustawienia długości fali</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6936"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select a configuration for setting band center wavelengths&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz konfigurację dla ustawień centralnej długości fali kanału&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6947"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create a virtual raster of the band set&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stwórz raster wirtualny zespołu kanałów&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6950"/>
        <source>Create virtual raster of band set</source>
        <translation>Stwórz raster wirtualny zespołu kanałów</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6957"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create a .tif raster stacking the bands of the band set&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stwórz zbiorczy raster (.tif) z Zestawu kanałów&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6960"/>
        <source>Create raster of band set (stack bands)</source>
        <translation>Stwórz raster wieokanałowy z zestawu kanałów</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6967"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Build band overviews (external pyramids) for faster visualization&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;/p&gt;Generuj kanały podglądu (piramidy zewnętrzne) w celu szybszej wizualizacji&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="6970"/>
        <source>Build band overviews</source>
        <translation>Generuj kanały podglądu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7002"/>
        <source>Settings</source>
        <translation type="unfinished">Ustawienia</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7020"/>
        <source>Interface</source>
        <translation type="unfinished">Interfejs</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7047"/>
        <source>Group name</source>
        <translation type="unfinished">Nazwa grupy</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7063"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Group name&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nazwa grupy&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7066"/>
        <source>Class_temp_group</source>
        <translation type="unfinished">grupa_tym_klas</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7082"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset group name to default&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zmień nazwę grupy na domyślną&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7179"/>
        <source>Reset name</source>
        <translation type="unfinished">Zresetuj nazwę</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7108"/>
        <source> Temporary group name</source>
        <translation type="unfinished">Tymczasowa nazwa grupy</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7141"/>
        <source> Variable name</source>
        <translation>Nazwa zmiennej</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7157"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Variable name for expressions&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nazwa zmiennej do wyrażenia&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7160"/>
        <source>raster</source>
        <translatorcomment>ERROR IN SOURCE! should be *reset*</translatorcomment>
        <translation>zresetuj</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7176"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset variable name to default&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zresetuj zmienną nazwę na dymyślną&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7202"/>
        <source> Variable name for expressions (tab Reclassification)</source>
        <translation type="unfinished">Zmienna nazwa dla wyrażenia (zakładka Ponowna klasyfikacja)</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7222"/>
        <source>C Info field</source>
        <translation type="unfinished">Pole C Info</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7229"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset field names to default&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zresetuj nazwę pola na podstawową&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7232"/>
        <source>Reset field names</source>
        <translation type="unfinished">Zresetuj nazwę pola</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7239"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the Class information field name&lt;/p&gt;&lt;p&gt;[max 10 characters]&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ustaw nazwę Pola informacji o klasyfikacji&lt;/p&gt;&lt;p&gt;[maks 10 znaków]&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7265"/>
        <source> Field names of training shapefile</source>
        <translation>Nazwy pól shapefile z polami treningowymi</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7275"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the Class ID field name&lt;/p&gt;&lt;p&gt;[max 10 characters]&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ustaw nazwę pola ID klasyfikacji&lt;/p&gt;&lt;p&gt;[maks 10 znaków]&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7285"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the Macroclass ID field name&lt;/p&gt;&lt;p&gt;[max 10 characters]&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ustaw nazwę pola  ID makroklasy&lt;/p&gt;&lt;p&gt;[maks 10 znaków]&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7301"/>
        <source>C ID field</source>
        <translation type="unfinished">Pole C ID</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7308"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the Macroclass information field name&lt;/p&gt;&lt;p&gt;[max 10 characters]&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ustaw nazwę pola Informacji makroklasy&lt;/p&gt;&lt;p&gt;[maks 10 znaków]&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7324"/>
        <source>MC ID field</source>
        <translation type="unfinished">Pole MC ID</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7337"/>
        <source>MC Info field</source>
        <translation type="unfinished">Pole MC Info</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7388"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Change ROI transparency&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>obszar treningowy</translatorcomment>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zmień przezroczystość OT&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7431"/>
        <source>Transparency</source>
        <translation type="unfinished">Przezroczystość</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7454"/>
        <source> ROI style</source>
        <translation type="unfinished">Styl OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7470"/>
        <source>ROI colour</source>
        <translation type="unfinished">Kolor OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7477"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select ROI colour&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz kolor OT&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7480"/>
        <source>Change colour</source>
        <translation type="unfinished">Zmień kolor</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7487"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset ROI style to default&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zresetuj styl OT na podstawowy&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7490"/>
        <source>Reset ROI style</source>
        <translation type="unfinished">Zresetuj styl OT</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7520"/>
        <source> Plot legend Max number of characters</source>
        <translation>Maks liczba znaków w legendzie</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7539"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Text lenght of names in the spectral plot legend&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;justify&quot;&gt;Długość tekstu w legendzie wykresu spektralnego&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7574"/>
        <source> Spectral signature</source>
        <translation type="unfinished">Charakterystyka spektralna</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7599"/>
        <source>Processing</source>
        <translation type="unfinished">Obróbka obrazu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7607"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enable/Disable the sound when the process is finished&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Włącz/Wyłącz dźwięk po zakończeniu obróbki obrazu&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7610"/>
        <source>Play sound when finished</source>
        <translation>Odtwórz dźwięk po zakończeniu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7623"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If enabled, the rasters calculated by the classification algorithm (one per signature) are saved along with the classification&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Kiedy włączone, rastry wyznaczone przez algorytm klasyfikacji (jeden na sygnaturę) zostaną zapisane wraz z klasyfikacją&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7626"/>
        <source>Save algorithm files</source>
        <translation type="unfinished">Zapisz pliki algorytmu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7633"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, create virtual rasters for certain temporary files&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Jeżeli zaznaczone, stwórz rastry wirtualne dla plików tymczasowych&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7636"/>
        <source>Use virtual raster for temp files</source>
        <translatorcomment>tymczasowych</translatorcomment>
        <translation>Użyj rastrów wirtualnych dla plików tym</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7643"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, a lossless compression is applied to rasters in order to save disk space&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Jeżeli zaznaczone, zostanie zastosowana kompresja bezstratna dla rastrów w celu zaoszczędzenia miejsca na dysku&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7646"/>
        <source>Raster compression</source>
        <translation>Kompresja rastrów</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7666"/>
        <source>Classification process</source>
        <translation type="unfinished">Proces klasyfikacji</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7683"/>
        <source>Raster data type for image calculations</source>
        <translation type="unfinished">Typ danych rastra do wyznaczania obrazu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7700"/>
        <source>Image calculation</source>
        <translation>Przetwarzanie obrazu</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7720"/>
        <source>Float32</source>
        <translation type="unfinished">Float32</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7725"/>
        <source>Float64</source>
        <translation type="unfinished">Float64</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7747"/>
        <source> RAM</source>
        <translation type="unfinished"> RAM</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7774"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set available RAM for processes&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ustaw dostępną pamięć RAM do obróbki obrazu&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7802"/>
        <source>Available RAM (MB)</source>
        <translation type="unfinished">Dostępny RAM (MB)</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7834"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select a new temporary directory where temporary files are saved during processing&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wybierz nowy katalog tymczasowy do zapisywania plików tymczasowych podczas obróbki obrazu&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7837"/>
        <source>Change directory</source>
        <translation type="unfinished">Zmień katalog</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7850"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset to default temporary directory&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zresetuj katalog tymczasowy na domyślny&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7853"/>
        <source>Reset to default</source>
        <translation>Zresetuj na domyślny</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7870"/>
        <source>Temporary directory</source>
        <translation type="unfinished">Katalog tymczasowy</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7901"/>
        <source>Debug</source>
        <translation>Śledź błędy</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7909"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enable/Disable the Log of events&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Włącz/Wyłącz logowanie w pliku zdarzeń&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7912"/>
        <source>Record events in a Log file</source>
        <translation>Rejestruj zdarzenia w pliku</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7925"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Export the Log file&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Eksportuj Plik zdarzeń&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7928"/>
        <source>Export Log file</source>
        <translation>Eksportuj Plik zdarzeń</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7935"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Clear the Log file content&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wyczyść zawartość Pliku zdarzeń&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7938"/>
        <source>Clear Log file content</source>
        <translation>Wyczyść zawartość Pliku zdarzeń</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7955"/>
        <source> Log file</source>
        <translation>Plik zdarzeń</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7976"/>
        <source> Test</source>
        <translation type="unfinished"> Test</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7983"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Test dependencies&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Testuj zależności&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="7986"/>
        <source>Test dependencies</source>
        <translation>Testuj zależności</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="8026"/>
        <source>About</source>
        <translation>O wtyczce</translation>
    </message>
    <message utf8="true">
        <location filename="ui_semiautomaticclassificationplugin.ui" line="8096"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Droid Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot; bgcolor=&quot;#f2f1f0&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Written by &lt;/span&gt;&lt;a href=&quot;http://www.researchgate.net/profile/Luca_Congedo&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#0057ae;&quot;&gt;Luca Congedo&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;  (ing.congedoluca@gmail.com), the &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Semi-Automatic Classification Plugin&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; (SCP) is a free open source plugin for QGIS that allows for the &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;semi-automatic classification&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; (also supervised classification) of remote sensing images. Also, it provides several tools for the pre processing of images, the post processing of classifications, and the raster calculation.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;SCP allows for the rapid creation of &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;ROIs&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; (training areas), through &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;region growing&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; algorithm, which are stored in a shapefile. The &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;scatter plot &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;or ROIs is available. Spectral signatures of training areas are calculated automatically, and can be displayed in a&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt; spectral signature plot &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;along with the values thereof. Spectral distances among signatures (e.g. Jeffries Matusita distance, or spectral angle) can be calculated for assessing &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;spectral separability&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;. Spectral signatures can be exported and imported from external sources. Also, a tool allows for the selection and &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;download of spectral signatures&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; from the USGS Spectral Library .&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The following tools are available for the &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;pre processing &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;of images: &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;automatic Landsat conversion to surface reflectance&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;, &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;clipping&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; multiple rasters, and &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;splitting&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; multi-band rasters.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;classification algorithms&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; available are: Minimum Distance, Maximum Likelihood, Spectral Angle Mapping. SCP allows for interactive &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;preview of classification&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;post processing&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; tools include: &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;accuracy assessment&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;, land cover change, classification report, classification to vector, reclassification of raster values. Also, a band calc tool allows for the &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;raster calculation &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;using NumPy functions.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;This plugin requires the installation of GDAL, OGR, Numpy, SciPy, and Matplotlib.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-style:italic;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;For more information and tutorials visit the official site &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;From GIS to Remote Sensing.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/plugins/semiautomaticclassificationplugin/icons/fromGIStoRS.png&quot; /&gt;&lt;a href=&quot;http://fromgistors.blogspot.com/p/semi-automatic-classification-plugin.html?spref=sacp&quot;&gt;&lt;span style=&quot; font-size:14pt; text-decoration: underline; color:#0000ff;&quot;&gt;From GIS to Remote Sensing&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;a href=&quot;https://www.facebook.com/groups/661271663969035&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;Semi-Automatic Classification Plugin group on Facebook&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://plus.google.com/communities/107833394986612468374&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;Semi-Automatic Classification Plugin community on Google+&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;&lt;br /&gt;The Semi-Automatic Classification Plugin is developed by Luca Congedo.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;The first version of the plugin was developed by Luca Congedo for the Adapting to Climate Change in Coastal Dar es Salaam Project &lt;/span&gt;&lt;a href=&quot;http://www.planning4adaptation.eu/&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.planning4adaptation.eu/&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; . Thanks go to Michele Munafò for his valuable advice.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:6px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Semi-Automatic Classification Plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Semi-Automatic Classification Plugin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with Semi-Automatic Classification Plugin. If not, see &amp;lt;&lt;/span&gt;&lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.gnu.org/licenses/&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;&amp;gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Droid Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot; bgcolor=&quot;#f2f1f0&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Written by &lt;/span&gt;&lt;a href=&quot;http://www.researchgate.net/profile/Luca_Congedo&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#0057ae;&quot;&gt;Luca Congedo&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;  (ing.congedoluca@gmail.com), the &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Semi-Automatic Classification Plugin&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; (SCP) is a free open source plugin for QGIS that allows for the &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;semi-automatic classification&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; (also supervised classification) of remote sensing images. Also, it provides several tools for the pre processing of images, the post processing of classifications, and the raster calculation.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;SCP allows for the rapid creation of &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;ROIs&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; (training areas), through &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;region growing&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; algorithm, which are stored in a shapefile. The &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;scatter plot &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;or ROIs is available. Spectral signatures of training areas are calculated automatically, and can be displayed in a&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt; spectral signature plot &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;along with the values thereof. Spectral distances among signatures (e.g. Jeffries Matusita distance, or spectral angle) can be calculated for assessing &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;spectral separability&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;. Spectral signatures can be exported and imported from external sources. Also, a tool allows for the selection and &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;download of spectral signatures&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; from the USGS Spectral Library .&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The following tools are available for the &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;pre processing &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;of images: &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;automatic Landsat conversion to surface reflectance&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;, &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;clipping&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; multiple rasters, and &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;splitting&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; multi-band rasters.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;classification algorithms&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; available are: Minimum Distance, Maximum Likelihood, Spectral Angle Mapping. SCP allows for interactive &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;preview of classification&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;post processing&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; tools include: &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;accuracy assessment&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;, land cover change, classification report, classification to vector, reclassification of raster values. Also, a band calc tool allows for the &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;raster calculation &lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;using NumPy functions.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-style:italic;&quot;&gt;This plugin requires the installation of GDAL, OGR, Numpy, SciPy, and Matplotlib.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; font-style:italic;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;For more information and tutorials visit the official site &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;From GIS to Remote Sensing.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/plugins/semiautomaticclassificationplugin/icons/fromGIStoRS.png&quot; /&gt;&lt;a href=&quot;http://fromgistors.blogspot.com/p/semi-automatic-classification-plugin.html?spref=sacp&quot;&gt;&lt;span style=&quot; font-size:14pt; text-decoration: underline; color:#0000ff;&quot;&gt;From GIS to Remote Sensing&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;a href=&quot;https://www.facebook.com/groups/661271663969035&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;Semi-Automatic Classification Plugin group on Facebook&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://plus.google.com/communities/107833394986612468374&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;Semi-Automatic Classification Plugin community on Google+&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;hr /&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;&lt;br /&gt;The Semi-Automatic Classification Plugin is developed by Luca Congedo.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;The first version of the plugin was developed by Luca Congedo for the Adapting to Climate Change in Coastal Dar es Salaam Project &lt;/span&gt;&lt;a href=&quot;http://www.planning4adaptation.eu/&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.planning4adaptation.eu/&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; . Thanks go to Michele Munafò for his valuable advice.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:6px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Semi-Automatic Classification Plugin is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Semi-Automatic Classification Plugin is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with Semi-Automatic Classification Plugin. If not, see &amp;lt;&lt;/span&gt;&lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.gnu.org/licenses/&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;&amp;gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="8142"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Online help&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pomoc Online&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="8145"/>
        <source>Online help</source>
        <translation type="unfinished">Pomoc Online</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="8156"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Quick guide&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Szybka instrukcja&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="8159"/>
        <source>Quick user guide</source>
        <translation type="unfinished">Szybka instrukcja użytkowania</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="8180"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show docks&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pokaż dokumenty&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin.ui" line="8183"/>
        <source>Show docks</source>
        <translation type="unfinished">Pokaż dokumenty</translation>
    </message>
</context>
<context>
    <name>SpectralSignaturePlot</name>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="20"/>
        <source>SCP: Spectral Signature Plot</source>
        <translation type="unfinished">SCP: Wykres Charakterystyki Spektralnej</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="48"/>
        <source> Signature list</source>
        <translation type="unfinished">Lista sygnatur</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="90"/>
        <source>S</source>
        <translation type="unfinished">S</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="95"/>
        <source>MC ID</source>
        <translation type="unfinished">MC ID</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="100"/>
        <source>MC Info</source>
        <translation type="unfinished">MC Info</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="105"/>
        <source>C ID</source>
        <translation type="unfinished">C ID</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="110"/>
        <source>C Info</source>
        <translation type="unfinished">C Info</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="115"/>
        <source>Color</source>
        <translation type="unfinished">Kolor</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="125"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Plot standard deviation for each signature&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wykres odchylenia standardowego dla każdej sygnatury&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message utf8="true">
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="128"/>
        <source>Plot σ</source>
        <translation type="unfinished">Wykres σ</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="135"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove highlighted signatures from list&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Usuń wybrane sygnatury z listy&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="138"/>
        <source>Remove signatures</source>
        <translation type="unfinished">Usuń sygnatury</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="145"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically fit the plot to data&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatycznie wypełnij wykres danymi&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="148"/>
        <source>Fit plot to data</source>
        <translation type="unfinished">Dopasuj wykres do danych</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="155"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Calculate spectral distances between signatures (Jeffries-Matusita distance, Euclidean distance, Spectral angle, etc.)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wyznacz odległość spektralną pomiędzy sygnaturami (odległość Jeffries&apos;a-Matusita,odległość euklidesowa, Kąt Spektralny, itp.)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="158"/>
        <source>Calculate spectral distances</source>
        <translation type="unfinished">Wyznacz odległości spektralne</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="192"/>
        <source>Plot</source>
        <translation type="unfinished">Wykres</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="221"/>
        <source>Signature details</source>
        <translation type="unfinished">Szczegóły sygnatury</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_signature_plot.ui" line="239"/>
        <source>Spectral distances</source>
        <translation type="unfinished">Odległości spektralne</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_welcome.ui" line="32"/>
        <source>Welcome</source>
        <translation>Witaj</translation>
    </message>
    <message>
        <location filename="ui_semiautomaticclassificationplugin_welcome.ui" line="54"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Welcome to the Semi-Automatic Classification Plugin for QGIS&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/plugins/semiautomaticclassificationplugin/icons/fromGIStoRS.png&quot; /&gt;  &lt;a href=&quot;http://fromgistors.blogspot.com/?spref=scp&quot;&gt;&lt;span style=&quot; font-weight:600; text-decoration: underline; color:#0057ae;&quot;&gt;From GIS to Remote Sensing&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600; text-decoration: underline; color:#0057ae;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600; text-decoration: underline; color:#0057ae;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Please, visit the &lt;a href=&quot;http://fromgistors.blogspot.com/p/theinterface-2.html?spref=scp&quot;&gt;&lt;span style=&quot; font-weight:600; text-decoration: underline; color:#0057ae;&quot;&gt;user manual page&lt;/span&gt;&lt;/a&gt; for information about the plugin interface.&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Also, several &lt;a href=&quot;http://fromgistors.blogspot.com/search/label/Tutorial/?spref=scp&quot;&gt;&lt;span style=&quot; font-weight:600; text-decoration: underline; color:#0057ae;&quot;&gt;tutorials&lt;/span&gt;&lt;/a&gt; are available about the use of this plugin.&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://www.facebook.com/groups/661271663969035/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;Semi-Automatic Classification Plugin group in Facebook&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; text-decoration: underline; color:#0057ae;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://plus.google.com/u/0/communities/107833394986612468374&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;Semi-Automatic Classification Plugin community in Google+&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Welcome to the Semi-Automatic Classification Plugin for QGIS&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/plugins/semiautomaticclassificationplugin/icons/fromGIStoRS.png&quot; /&gt;  &lt;a href=&quot;http://fromgistors.blogspot.com/?spref=scp&quot;&gt;&lt;span style=&quot; font-weight:600; text-decoration: underline; color:#0057ae;&quot;&gt;From GIS to Remote Sensing&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600; text-decoration: underline; color:#0057ae;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600; text-decoration: underline; color:#0057ae;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Please, visit the &lt;a href=&quot;http://fromgistors.blogspot.com/p/theinterface-2.html?spref=scp&quot;&gt;&lt;span style=&quot; font-weight:600; text-decoration: underline; color:#0057ae;&quot;&gt;user manual page&lt;/span&gt;&lt;/a&gt; for information about the plugin interface.&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Also, several &lt;a href=&quot;http://fromgistors.blogspot.com/search/label/Tutorial/?spref=scp&quot;&gt;&lt;span style=&quot; font-weight:600; text-decoration: underline; color:#0057ae;&quot;&gt;tutorials&lt;/span&gt;&lt;/a&gt; are available about the use of this plugin.&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://www.facebook.com/groups/661271663969035/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;Semi-Automatic Classification Plugin group in Facebook&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; text-decoration: underline; color:#0057ae;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://plus.google.com/u/0/communities/107833394986612468374&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;Semi-Automatic Classification Plugin community in Google+&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>semiautomaticclassificationplugin</name>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="1445"/>
        <source>Please, restart QGIS for executing the Semi-Automatic Classification Plugin</source>
        <translation type="unfinished">Proszę ponownie uruchomić QGIS aby uzyskać dostęp do Semi-Automatic Classification Plugin</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="258"/>
        <source>SCP</source>
        <translation type="unfinished">SCP</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="487"/>
        <source>Tools</source>
        <translation type="unfinished">Narzędzia</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="493"/>
        <source>Pre processing</source>
        <translation type="unfinished">Obróbka wstępna</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="499"/>
        <source>Post processing</source>
        <translation type="unfinished">Obróbka końcowa</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="511"/>
        <source>Settings</source>
        <translation type="unfinished">Ustawienia</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="400"/>
        <source>Semi-Automatic Classification Plugin</source>
        <translation type="unfinished">Semi-Automatic Classification Plugin</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="415"/>
        <source>Band set</source>
        <translation type="unfinished">Zestaw kanałów</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="431"/>
        <source>Select an image</source>
        <translation type="unfinished">Wybierz obraz</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="435"/>
        <source>Refresh list</source>
        <translation type="unfinished">Odśwież listę</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="453"/>
        <source>Select a RGB color composite</source>
        <translation type="unfinished">Wybierz kompozycję kolorów RGB</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="461"/>
        <source>Show/hide the input image</source>
        <translation type="unfinished">Pokaż/ukryj obraz wejściowy</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="469"/>
        <source>Local cumulative cut stretch of band set</source>
        <translation>Lokalnie, skumulowane wzmocnienie kontrastu</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="475"/>
        <source>Local standard deviation stretch of band set</source>
        <translation>Lokalne wzmocnienie kontrastu (odchylenie standardowe)</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="481"/>
        <source>Spectral plot</source>
        <translation type="unfinished">Wykres spektralny</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="505"/>
        <source>Band calc</source>
        <translation type="unfinished">Wyzn Kanałów</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="517"/>
        <source>User guide</source>
        <translation type="unfinished">Instrukcja użytkowania</translation>
    </message>
    <message>
        <location filename="semiautomaticclassificationplugin.py" line="523"/>
        <source>Online help</source>
        <translation type="unfinished">Pomoc Online</translation>
    </message>
    <message>
        <location filename="classificationdock.py" line="287"/>
        <source>Select a mask shapefile</source>
        <translation type="unfinished">Wybierz shapefile maski</translation>
    </message>
    <message>
        <location filename="classificationdock.py" line="495"/>
        <source>Save classification output</source>
        <translation type="unfinished">Zapisz wyjście klasyfikacji</translation>
    </message>
    <message>
        <location filename="classificationdock.py" line="587"/>
        <source> conversion to vector. Please wait ...</source>
        <translation type="unfinished"> konwersja na wektor. Prosze czekać ...</translation>
    </message>
    <message>
        <location filename="classificationdock.py" line="672"/>
        <source>Select a qml style</source>
        <translation type="unfinished">Wybierz styl qml</translation>
    </message>
    <message>
        <location filename="classificationdock.py" line="773"/>
        <source>Select a signature list file</source>
        <translation type="unfinished">Wybierz plik z listą sygnatur</translation>
    </message>
    <message>
        <location filename="classificationdock.py" line="835"/>
        <source>Export the signature list to file</source>
        <translation type="unfinished">Eksportuj listę sygnatur do pliku</translation>
    </message>
    <message>
        <location filename="downloadsentinelimages.py" line="458"/>
        <source>Reset signature list</source>
        <translation type="unfinished">Zresetuj listę sygnatur</translation>
    </message>
    <message>
        <location filename="classificationdock.py" line="885"/>
        <source>Are you sure you want to reset signature list?</source>
        <translation type="unfinished">Czy na pewno chcesz zresetować listę sygnatur?</translation>
    </message>
    <message>
        <location filename="classificationdock.py" line="894"/>
        <source>Select a library file</source>
        <translation type="unfinished">Wybierz bibliotekę pliku</translation>
    </message>
    <message>
        <location filename="classificationdock.py" line="921"/>
        <source>Export the highlighted signatures to CSV library</source>
        <translation type="unfinished">Eksportuj wybrane sygnatury do biblioteki CSV</translation>
    </message>
    <message>
        <location filename="spectralsignatureplot.py" line="152"/>
        <source>Delete signatures</source>
        <translation type="unfinished">Usuń sygnatury</translation>
    </message>
    <message>
        <location filename="spectralsignatureplot.py" line="152"/>
        <source>Are you sure you want to delete highlighted signatures?</source>
        <translation type="unfinished">Czy na pewno chcesz usunąć wybrane sygnatury?</translation>
    </message>
    <message>
        <location filename="classificationdock.py" line="1082"/>
        <source>Merge signatures</source>
        <translation type="unfinished">Połącz sygnatury</translation>
    </message>
    <message>
        <location filename="classificationdock.py" line="1082"/>
        <source>Merge highlighted signatures?</source>
        <translation type="unfinished">Połączyć wybrane sygnatury?</translation>
    </message>
    <message>
        <location filename="messages.py" line="77"/>
        <source>Test results</source>
        <translation type="unfinished">Wyniki testu</translation>
    </message>
    <message>
        <location filename="messages.py" line="125"/>
        <source>Information</source>
        <translation type="unfinished">Informacja</translation>
    </message>
    <message>
        <location filename="messages.py" line="80"/>
        <source>Default ROI style</source>
        <translation type="unfinished">Podstawowy styl OT</translation>
    </message>
    <message>
        <location filename="messages.py" line="83"/>
        <source>No log file found</source>
        <translation>Nie znaleziono Pliku zdarzeń</translation>
    </message>
    <message>
        <location filename="messages.py" line="86"/>
        <source>Select a shapefile; shapefile is not loaded</source>
        <translation type="unfinished">Wybierz shapefile; shapefile nie jest załadowany</translation>
    </message>
    <message>
        <location filename="messages.py" line="89"/>
        <source>Select a raster; raster is not loaded</source>
        <translation type="unfinished">Wybierz raster; raster nie jest załadowany</translation>
    </message>
    <message>
        <location filename="messages.py" line="92"/>
        <source>Select a point inside the image area</source>
        <translation type="unfinished">Wybierz punkt w obszarze obrazu</translation>
    </message>
    <message>
        <location filename="messages.py" line="95"/>
        <source>No file found</source>
        <translation type="unfinished">Nie znaleziono pliku</translation>
    </message>
    <message>
        <location filename="messages.py" line="98"/>
        <source>Data projections do not match. Reproject data to the same projection</source>
        <translation>Układy współrzędnych nie są zgodne. Dokonaj reprojekcji</translation>
    </message>
    <message>
        <location filename="messages.py" line="101"/>
        <source>Maximum Likelihood threshold must be less than 100</source>
        <translation type="unfinished">Maksymalny Próg Prawdopodobieństwa musi być mniejszy niż 100</translation>
    </message>
    <message>
        <location filename="messages.py" line="104"/>
        <source>Spectral Angle Mapping threshold must be less than 90</source>
        <translation type="unfinished">Próg Mapowania Kąta Spektralnego musi być mniejszy niż 90</translation>
    </message>
    <message>
        <location filename="messages.py" line="107"/>
        <source>Select a classification output</source>
        <translation type="unfinished">Wybierz wyjście klasyfikacji</translation>
    </message>
    <message>
        <location filename="splitTab.py" line="90"/>
        <source>Select a directory</source>
        <translation type="unfinished">Wybierz katalog</translation>
    </message>
    <message>
        <location filename="messages.py" line="113"/>
        <source>Restart QGIS for the new settings</source>
        <translation type="unfinished">Aby wprowadzić nowe ustawienia należy zrestartować QGIS</translation>
    </message>
    <message>
        <location filename="messages.py" line="116"/>
        <source>At least 3 points are required</source>
        <translation type="unfinished">Wymagane są przynajmniej 3 punkty</translation>
    </message>
    <message>
        <location filename="messages.py" line="119"/>
        <source>Negative IDs are not allowed</source>
        <translation type="unfinished">Ujemne ID nie są dozwolone</translation>
    </message>
    <message>
        <location filename="messages.py" line="122"/>
        <source>Select at least one signature</source>
        <translation type="unfinished">Wybierz przynajmniej jedną sygnaturę</translation>
    </message>
    <message>
        <location filename="messages.py" line="125"/>
        <source>SCP is recording the Log file</source>
        <translation>SCP zapisuje Plik zdarzeń</translation>
    </message>
    <message>
        <location filename="downloadsentinelimages.py" line="237"/>
        <source>Error</source>
        <translation type="unfinished">Błąd</translation>
    </message>
    <message>
        <location filename="messages.py" line="129"/>
        <source>Classification failed.</source>
        <translation type="unfinished">Klasyfikacja nieudana.</translation>
    </message>
    <message>
        <location filename="messages.py" line="132"/>
        <source>ROI creation failed. 
or 
Possible reason: one or more band of the band set are missing</source>
        <translation type="unfinished">Tworzenie OT nie powiodło się. 
lub 
Prawdopodobny powód:brakuje jednego lub więcej kanałów z zestawu kanałów</translation>
    </message>
    <message>
        <location filename="messages.py" line="138"/>
        <source>Signature calculation failed. 
Possible reason: the raster is not loaded</source>
        <translation type="unfinished">Wyznaczanie sygnatur nie powiodło się. 
Prawdopodobny powód: raster nie jest załadowany</translation>
    </message>
    <message>
        <location filename="messages.py" line="141"/>
        <source>Import failed. 
Possible reason: selected file is not a band set</source>
        <translation type="unfinished">Importowanie nie powiodło się. 
Prawdopodobny powód: Wybrany plik nie jest zestawem kanałów</translation>
    </message>
    <message>
        <location filename="messages.py" line="144"/>
        <source>Classification failed. 
It appears the one or more bands of the band set are missing</source>
        <translation type="unfinished">Klasyfikacja nie powiodła się. 
Wygląda na to, że brakuje jednego lub więcej kanałów z zestawu kanałów</translation>
    </message>
    <message>
        <location filename="messages.py" line="147"/>
        <source>ROI creation failed. 
Possible reason: input is a virtual raster or band is not loaded</source>
        <translation type="unfinished">Tworzenie OT nie powiodło się. 
Prawdopodobny powód: Wejście jest rastrem wirtualnym lub kanał nie jest załadowany</translation>
    </message>
    <message>
        <location filename="messages.py" line="150"/>
        <source>No metadata found inside the input directory (a .txt file whose name contains MTL)</source>
        <translation type="unfinished">Nie znaleziono metadanych w katalogu wejścia (pliku tekstowego, który w nazwie zawiera MTL)</translation>
    </message>
    <message>
        <location filename="messages.py" line="153"/>
        <source>Raster not found</source>
        <translation type="unfinished">Nie znaleziono rastra</translation>
    </message>
    <message>
        <location filename="messages.py" line="156"/>
        <source>Raster not found or clip failed</source>
        <translation type="unfinished">Nie znaleziono rastra lub złączanie nie powiodło się</translation>
    </message>
    <message>
        <location filename="messages.py" line="159"/>
        <source>Shapefile or raster not found</source>
        <translation type="unfinished">Nie znaleziono shapefile lub rastra</translation>
    </message>
    <message>
        <location filename="messages.py" line="162"/>
        <source>Error deleting ROI</source>
        <translation>Błąd przy usuwaniu OT</translation>
    </message>
    <message>
        <location filename="messages.py" line="165"/>
        <source>The Macroclass field is missing</source>
        <translation type="unfinished">Nie znaleziono pola Makroklasa</translation>
    </message>
    <message>
        <location filename="messages.py" line="168"/>
        <source>Error saving signatures</source>
        <translation type="unfinished">Błąd zapisu sygnatur</translation>
    </message>
    <message>
        <location filename="messages.py" line="171"/>
        <source>Error opening signatures</source>
        <translation type="unfinished">Błąd otwierania sygnatur</translation>
    </message>
    <message>
        <location filename="messages.py" line="174"/>
        <source>Error opening spectral library</source>
        <translation>Błąd otwierania biblioteki spektralnej</translation>
    </message>
    <message>
        <location filename="messages.py" line="177"/>
        <source>Error saving spectral library</source>
        <translation type="unfinished">Błąd zapisywania biblioteki spektralnej</translation>
    </message>
    <message>
        <location filename="messages.py" line="180"/>
        <source>Import failed</source>
        <translation type="unfinished">Importowanie nie powiodło się</translation>
    </message>
    <message>
        <location filename="messages.py" line="183"/>
        <source>ROI creation failed</source>
        <translation type="unfinished">Tworzenie OT nie powiodło się</translation>
    </message>
    <message>
        <location filename="messages.py" line="186"/>
        <source>Internet connection failed</source>
        <translation type="unfinished">Łączenie z internetem nie powiodło się</translation>
    </message>
    <message>
        <location filename="messages.py" line="189"/>
        <source>Exporting Log file failed</source>
        <translation>Eksportowanie Pliku zdarzeń nie powiodło się</translation>
    </message>
    <message>
        <location filename="messages.py" line="192"/>
        <source>Saving algorithm files failed</source>
        <translation type="unfinished">Zapisywanie pliku algorytmu nie powiodło się</translation>
    </message>
    <message>
        <location filename="messages.py" line="195"/>
        <source>Unable to get ROI attributes; check training shapefiles field names</source>
        <translation type="unfinished">Nie można uzyskać atrybutów OT; sprawdź nazwy treningowych plików shapefile</translation>
    </message>
    <message>
        <location filename="messages.py" line="201"/>
        <source>Error reading raster. Possibly the raster path contains unicode characters</source>
        <translation type="unfinished">Błąd odczytu rastra. Możliwe, że ścieżka dostępu rastra zawiera znaki UNICODE</translation>
    </message>
    <message>
        <location filename="messages.py" line="204"/>
        <source>The version of Numpy is outdated. Please install QGIS using OSGEO4W for an updated version of Numpy or visit http://fromgistors.blogspot.com/p/frequently-asked-questions.html#numpy_version</source>
        <translation type="unfinished">Wersja Numpy jest przestarzała. Proszę zainstalować QGIS wykorzystując OSGEO4W w celu zaktualizowania wersji Numpy albo odwiedź stronę http://fromgistors.blogspot.com/p/frequently-asked-questions.html#numpy_version</translation>
    </message>
    <message>
        <location filename="messages.py" line="207"/>
        <source>Unable to perform operation. Possibly OGR is missing drivers. Please repeat QGIS installation.</source>
        <translation>Nie można wykonać działania. Możliwe, że brakuje OGR. Proszę ponownie zainstalować QGIS.</translation>
    </message>
    <message>
        <location filename="messages.py" line="210"/>
        <source>Memory error. Please, set a lower value of RAM in the tab Settings.</source>
        <translation type="unfinished">Błąd pamięci. Proszę ustawić mniejszą wartość RAM&apos;u w zakładce Ustawienia.</translation>
    </message>
    <message>
        <location filename="messages.py" line="213"/>
        <source>Edge error. Reduce the ROI width or draw a ROI manually (recommended installation of GDAL &gt;= 1.10)</source>
        <translation type="unfinished">Błąd krawędzi. Zmniejsz szerokość OT lub narysuj OT ręcznie (zalecana instalacja GDAL &gt;= 1.10)</translation>
    </message>
    <message>
        <location filename="messages.py" line="216"/>
        <source>Unable to proceed. Rename the Landsat bands with a file name ending with the band number (e.g. rename B20 to B2)</source>
        <translation>Nie można kontynuować. Dopasuj koncówki nazw plików do numerów kanałów Landsat (np. zmień nazwę z B20 na B2)</translation>
    </message>
    <message>
        <location filename="messages.py" line="219"/>
        <source>Error calculating signature. Possibly ROI is too small</source>
        <translation type="unfinished">Błąd wyznaczania sygnatur. Prawdopodobnie OT jest za mały</translation>
    </message>
    <message>
        <location filename="messages.py" line="222"/>
        <source>Unable to split bands</source>
        <translation type="unfinished">Nie można rozdzielić kanałów</translation>
    </message>
    <message>
        <location filename="messages.py" line="225"/>
        <source>Error reading band set. Possibly raster files are not loaded</source>
        <translation type="unfinished">Błąd odczytu zestawu kanałów. Prawdopodobnie pliki rastra nie są załadowane</translation>
    </message>
    <message>
        <location filename="messages.py" line="228"/>
        <source>Clip area outside image. Check the raster projection</source>
        <translation type="unfinished">Obszar złączania poza obrazem. Sprawdź układ wsp. rastra</translation>
    </message>
    <message>
        <location filename="messages.py" line="231"/>
        <source>Unable to merge. Signatures have different unit or wavelength</source>
        <translation type="unfinished">Nie można połączyć. Sygnatury posiadają różne jednostki długości fali</translation>
    </message>
    <message>
        <location filename="messages.py" line="234"/>
        <source>Unable to calculate. Expression error</source>
        <translation type="unfinished">Nie można obliczyć. Błąd wyrażenia</translation>
    </message>
    <message>
        <location filename="messages.py" line="237"/>
        <source>Unable to calculate. Metadata error</source>
        <translation type="unfinished">Nie można wyznaczyć. Błąd metadanych</translation>
    </message>
    <message>
        <location filename="messages.py" line="243"/>
        <source>Unable to find images</source>
        <translation type="unfinished">Nie można odnaleźć obrazów</translation>
    </message>
    <message>
        <location filename="messages.py" line="246"/>
        <source>Unable to connect</source>
        <translation type="unfinished">Nie można połączyć</translation>
    </message>
    <message>
        <location filename="messages.py" line="249"/>
        <source>Unable to load image</source>
        <translation type="unfinished">Nie można załadować obrazu</translation>
    </message>
    <message>
        <location filename="messages.py" line="255"/>
        <source>Attribute table error</source>
        <translation type="unfinished">Błąd atrybutów tabeli</translation>
    </message>
    <message>
        <location filename="messages.py" line="258"/>
        <source>Unable to pansharpen: missing bands </source>
        <translation type="unfinished">Nie można wykonać polecenia pansharp: nie znaleziono kanałów </translation>
    </message>
    <message>
        <location filename="messages.py" line="261"/>
        <source>Unable to calculate</source>
        <translation type="unfinished">Nie można wyznaczyć</translation>
    </message>
    <message>
        <location filename="messages.py" line="264"/>
        <source>Error reading raster. Possibly bands are not aligned</source>
        <translation>Błąd odczytu rastra. Prawdopodobnie kanały nie są dopasowane</translation>
    </message>
    <message>
        <location filename="messages.py" line="267"/>
        <source>Unable to get raster projection. Try to reproject the raster</source>
        <translation>Nie można odczytać projekcji rastra. Spróbuj wykonać reprojekcję raster</translation>
    </message>
    <message>
        <location filename="messages.py" line="270"/>
        <source>Error calculating accuracy. Possibly shapefile polygons are outside classification</source>
        <translation type="unfinished">Błąd wyznaczania dokładności. Prawdopodobnie poligony pliku shapefile znajdują się poza klasyfikacją</translation>
    </message>
    <message>
        <location filename="messages.py" line="273"/>
        <source>Unable to connect. Check user name and password</source>
        <translation type="unfinished">Nie można połączyć. Sprawdź nazwę użytkownika i hasło</translation>
    </message>
    <message>
        <location filename="messages.py" line="325"/>
        <source>Warning</source>
        <translation type="unfinished">Uwaga</translation>
    </message>
    <message>
        <location filename="messages.py" line="280"/>
        <source>It appears that SciPy is not correctly installed. Please, update QGIS </source>
        <translation type="unfinished">Wygląda na to,że SciPy nie jest zainstalowany poprawnie. Proszę zaktualizować QGIS </translation>
    </message>
    <message>
        <location filename="messages.py" line="283"/>
        <source>It appears that SciPy is not correctly installed. Please, see this page for information about SciPy installation </source>
        <translation type="unfinished">Wygląda na to, że SciPy nie jest zainstalowany poprawnie. Proszę poszukać informacji dotyczących instalacji SciPy na tej stronie</translation>
    </message>
    <message>
        <location filename="messages.py" line="286"/>
        <source>rasters have different pixel sizes that can lead to incorrect results. Please, consider to resample rasters to the same pixel size</source>
        <translation type="unfinished">rastry posiadają różne rozmiary pikseli, które mogą prowadzić do niepoprawnych wyników. Proszę rozważyć ponowne próbkowanie rastrów do tego samego rozmiaru pikseli</translation>
    </message>
    <message>
        <location filename="messages.py" line="289"/>
        <source>The same ID class has been already assigned to a different macrolass</source>
        <translation type="unfinished">Ta sama klasa ID została już przypisana do innej makroklasy</translation>
    </message>
    <message>
        <location filename="messages.py" line="292"/>
        <source>Wavelength already present</source>
        <translation type="unfinished">Długość fali jest już podana</translation>
    </message>
    <message>
        <location filename="messages.py" line="295"/>
        <source>Wavelength unit not provided</source>
        <translation>Jednostka długości fali nie jest podana</translation>
    </message>
    <message>
        <location filename="messages.py" line="304"/>
        <source>RAM value was too high. Value has been decreased automatically</source>
        <translation type="unfinished">Wartość RAM&apos;u jest zbyt duża. Wartość została obniżona automatycznie</translation>
    </message>
    <message>
        <location filename="messages.py" line="310"/>
        <source>Unable to load the virtual raster. Please create it manually</source>
        <translation>Nie można załadować rastra wirtualnego. Proszę stworzyć go ręcznie</translation>
    </message>
    <message>
        <location filename="messages.py" line="313"/>
        <source>Unable to proceed. The raster must be in projected coordinates</source>
        <translation>Nie można kontynuować. Raster musi znajdować się we współrzędnych odwzorowania</translation>
    </message>
    <message>
        <location filename="messages.py" line="316"/>
        <source>Select at least one raster</source>
        <translation type="unfinished">Zaznacz przynajmniej jeden raster</translation>
    </message>
    <message>
        <location filename="messages.py" line="319"/>
        <source>Incorrect expression</source>
        <translation type="unfinished">Nieprawidłowe wyrażenie</translation>
    </message>
    <message>
        <location filename="messages.py" line="322"/>
        <source>Unable to access the temporary directory</source>
        <translation type="unfinished">Nie można uzyskać dostępu do katalogu tymczasowego</translation>
    </message>
    <message>
        <location filename="messages.py" line="325"/>
        <source>Reduce the search area extent within 10 degrees of latitude and 10 degrees of longitude</source>
        <translation type="unfinished">Zmniejsz zakres obszaru wyszukiwania w obrębie 10 stopni szerokości geograficznej i 10 stopni długości geograficznej</translation>
    </message>
    <message>
        <location filename="utils.py" line="2728"/>
        <source>Please wait ...</source>
        <translation type="unfinished">Proszę czekać...</translation>
    </message>
    <message>
        <location filename="accuracy.py" line="82"/>
        <source>Save error matrix raster output</source>
        <translation type="unfinished">Zapisz błąd macierzy wyjścia rastra</translation>
    </message>
    <message>
        <location filename="accuracy.py" line="221"/>
        <source>Classification</source>
        <translation type="unfinished">Klasyfikacja</translation>
    </message>
    <message>
        <location filename="accuracy.py" line="221"/>
        <source>ErrMatrixCode</source>
        <translation type="unfinished">BłądKoduMacierzy</translation>
    </message>
    <message>
        <location filename="accuracy.py" line="241"/>
        <source>Reference</source>
        <translation type="unfinished">Odniesienie</translation>
    </message>
    <message>
        <location filename="landcoverchange.py" line="199"/>
        <source>PixelSum</source>
        <translation type="unfinished">PodsPiks</translation>
    </message>
    <message>
        <location filename="accuracy.py" line="240"/>
        <source>ERROR MATRIX</source>
        <translation type="unfinished">BŁĄD MACIERZY</translation>
    </message>
    <message>
        <location filename="accuracy.py" line="254"/>
        <source>Total</source>
        <translation type="unfinished">Całkowicie</translation>
    </message>
    <message>
        <location filename="accuracy.py" line="263"/>
        <source>Overall accuracy [%] = </source>
        <translation type="unfinished">Ogólna dokładność [%] = </translation>
    </message>
    <message>
        <location filename="accuracy.py" line="277"/>
        <source>Class </source>
        <translation type="unfinished">Klasyfikacja</translation>
    </message>
    <message>
        <location filename="accuracy.py" line="277"/>
        <source> producer accuracy [%] = </source>
        <translation type="unfinished">dokładność producenta [%] = </translation>
    </message>
    <message>
        <location filename="accuracy.py" line="277"/>
        <source> user accuracy [%] = </source>
        <translation type="unfinished">dokładność użytkowanika [%] = </translation>
    </message>
    <message>
        <location filename="accuracy.py" line="277"/>
        <source>Kappa hat = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="accuracy.py" line="280"/>
        <source>Kappa hat classification = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="algorithmWeightTab.py" line="99"/>
        <source>Reset weights</source>
        <translation type="unfinished">Zresetuj wagi</translation>
    </message>
    <message>
        <location filename="algorithmWeightTab.py" line="99"/>
        <source>Are you sure you want to reset weights?</source>
        <translation type="unfinished">Czy na pewno zresetować wagi?</translation>
    </message>
    <message>
        <location filename="reclassificationTab.py" line="75"/>
        <source>Save raster output</source>
        <translation type="unfinished">Zapisz wyjście rastra</translation>
    </message>
    <message>
        <location filename="bandsetTab.py" line="216"/>
        <source>Clear band set</source>
        <translation type="unfinished">Oczyść zestaw kanałów</translation>
    </message>
    <message>
        <location filename="bandsetTab.py" line="216"/>
        <source>Are you sure you want to clear the band set?</source>
        <translation type="unfinished">Czy na pewno oczyścić zestaw kanałów?</translation>
    </message>
    <message>
        <location filename="bandsetTab.py" line="414"/>
        <source>Save the band set to file</source>
        <translation type="unfinished">Zapisz zestaw kanałów w pliku</translation>
    </message>
    <message>
        <location filename="bandsetTab.py" line="447"/>
        <source>Select a band set file</source>
        <translation type="unfinished">Wybierz plik z zestawem kanałów</translation>
    </message>
    <message>
        <location filename="bandsetTab.py" line="719"/>
        <source>Remove band</source>
        <translation type="unfinished">Usuń kanał</translation>
    </message>
    <message>
        <location filename="bandsetTab.py" line="719"/>
        <source>Are you sure you want to remove the selected bands from band set?</source>
        <translation type="unfinished">Czy na pewno usunąć wybrane kanały z zestawu kanałów?</translation>
    </message>
    <message>
        <location filename="bandsetTab.py" line="765"/>
        <source>Save virtual raster</source>
        <translation>Zapisz raster wirtualny</translation>
    </message>
    <message>
        <location filename="bandsetTab.py" line="782"/>
        <source>Save raster</source>
        <translation type="unfinished">Zapisz raster</translation>
    </message>
    <message>
        <location filename="bandsetTab.py" line="826"/>
        <source>Build overviews</source>
        <translation type="unfinished">Zbuduj podglądy</translation>
    </message>
    <message>
        <location filename="bandsetTab.py" line="826"/>
        <source>Do you want to build the external overviews of bands?</source>
        <translation>Czy na pewno zbudować zewnętrzne podglądy na kanały?</translation>
    </message>
    <message>
        <location filename="bandsetTab.py" line="838"/>
        <source> building overviews</source>
        <translation> tworzenie podglądu</translation>
    </message>
    <message>
        <location filename="classreportTab.py" line="65"/>
        <source>report</source>
        <translation type="unfinished">raport</translation>
    </message>
    <message>
        <location filename="classreportTab.py" line="95"/>
        <source>Unknown</source>
        <translation type="unfinished">Nieznane</translation>
    </message>
    <message>
        <location filename="classreportTab.py" line="120"/>
        <source>Class</source>
        <translation type="unfinished">Klasyf</translation>
    </message>
    <message>
        <location filename="classreportTab.py" line="120"/>
        <source>Percentage %</source>
        <translation type="unfinished">Procent %</translation>
    </message>
    <message>
        <location filename="classreportTab.py" line="161"/>
        <source>Save classification report</source>
        <translation type="unfinished">Zapisz raport klasyfikacji</translation>
    </message>
    <message>
        <location filename="classtovectorTab.py" line="70"/>
        <source>Save shapefile output</source>
        <translation type="unfinished">Zapisz wyjście shapefile</translation>
    </message>
    <message>
        <location filename="clipmultiplerasters.py" line="129"/>
        <source>Select a directory where to save clipped rasters</source>
        <translation type="unfinished">Wybierz katalog do zapisu złączonych rastrów</translation>
    </message>
    <message>
        <location filename="downloadlandsatimages.py" line="246"/>
        <source>Update image database</source>
        <translation type="unfinished">Zaktualizuj bazę danych obrazów</translation>
    </message>
    <message>
        <location filename="downloadlandsatimages.py" line="246"/>
        <source>Are you sure you want to download the Landsat image database (requires internet connection)?</source>
        <translation type="unfinished">Czy na pewno pobrać bazę danych obrazów Landsat (wymaga połączenia z internetem)?</translation>
    </message>
    <message>
        <location filename="downloadsentinelimages.py" line="264"/>
        <source>Download the images in the table (requires internet connection)</source>
        <translation type="unfinished">Pobierz obrazy w tabeli (wymaga połączenia z internetem)</translation>
    </message>
    <message>
        <location filename="downloadsentinelimages.py" line="329"/>
        <source>Export download links</source>
        <translation type="unfinished">Eksportuj pobrane łącza</translation>
    </message>
    <message>
        <location filename="downloadsentinelimages.py" line="364"/>
        <source>Searching ...</source>
        <translation type="unfinished">Wyszukiwanie...</translation>
    </message>
    <message>
        <location filename="downloadlandsatimages.py" line="731"/>
        <source>Landsat database directory</source>
        <translation type="unfinished">Katalog bazy danych Landsat</translation>
    </message>
    <message>
        <location filename="downloadlandsatimages.py" line="731"/>
        <source>Are you sure you want to reset Landsat database directory?</source>
        <translation type="unfinished">Czy na pewno zresetować katalog bazy danych Landsat?</translation>
    </message>
    <message>
        <location filename="downloadlandsatimages.py" line="736"/>
        <source>Select Landsat database directory</source>
        <translation type="unfinished">Wybierz katalog bazy danych Landsat</translation>
    </message>
    <message>
        <location filename="downloadsentinelimages.py" line="458"/>
        <source>Are you sure you want to clear the table?</source>
        <translation type="unfinished">Czy na pewno oczyścić tabelę?</translation>
    </message>
    <message>
        <location filename="landcoverchange.py" line="109"/>
        <source>Save land cover change raster output</source>
        <translation type="unfinished">Zapisz zmiany rastra wyjściowego z pokryciem terenu</translation>
    </message>
    <message>
        <location filename="landcoverchange.py" line="199"/>
        <source>ChangeCode</source>
        <translation type="unfinished">ZmieńKod</translation>
    </message>
    <message>
        <location filename="landcoverchange.py" line="199"/>
        <source>ReferenceClass</source>
        <translation type="unfinished">OdniesienieKlasyf</translation>
    </message>
    <message>
        <location filename="landcoverchange.py" line="199"/>
        <source>NewClass</source>
        <translation type="unfinished">NowaKlasyf</translation>
    </message>
    <message>
        <location filename="landsatTab.py" line="64"/>
        <source>Select a MTL file</source>
        <translation type="unfinished">Wybierz plik MTL</translation>
    </message>
    <message>
        <location filename="multipleroiTab.py" line="228"/>
        <source>Save the point list to file</source>
        <translation type="unfinished">Zapisz listę punktów w pliku</translation>
    </message>
    <message>
        <location filename="settings.py" line="272"/>
        <source>Transparency </source>
        <translation type="unfinished">Przezroczystość</translation>
    </message>
    <message>
        <location filename="settings.py" line="103"/>
        <source>Save Log file</source>
        <translation>Zapisz Plik zdarzeń</translation>
    </message>
    <message>
        <location filename="settings.py" line="209"/>
        <source>Reset field names</source>
        <translation type="unfinished">Zresetuj nazwy pól</translation>
    </message>
    <message>
        <location filename="settings.py" line="209"/>
        <source>Are you sure you want to reset field names?</source>
        <translation type="unfinished">Czy na pewno zresetować nazwy pól?</translation>
    </message>
    <message>
        <location filename="settings.py" line="220"/>
        <source>Reset variable name</source>
        <translation>Zresetuj nazwę zmiennej</translation>
    </message>
    <message>
        <location filename="settings.py" line="220"/>
        <source>Are you sure you want to reset variable name?</source>
        <translation>Czy na pewno zresetować nazwę zmiennej?</translation>
    </message>
    <message>
        <location filename="settings.py" line="228"/>
        <source>Reset group name</source>
        <translation type="unfinished">Zresetuj nazwę grupy</translation>
    </message>
    <message>
        <location filename="settings.py" line="228"/>
        <source>Are you sure you want to reset group name?</source>
        <translation type="unfinished">Czy na pewno zresetować nazwę grupy?</translation>
    </message>
    <message>
        <location filename="settings.py" line="257"/>
        <source>Change temporary directory</source>
        <translation type="unfinished">Zmień tymczasowy katalog</translation>
    </message>
    <message>
        <location filename="settings.py" line="236"/>
        <source>Are you sure you want to change the temporary directory?</source>
        <translation type="unfinished">Czy na pewno zmienić tymczasowy katalog?</translation>
    </message>
    <message>
        <location filename="settings.py" line="257"/>
        <source>Are you sure you want to reset the temporary directory?</source>
        <translation>Czy na pewno zresetować katalog tymczasowy?</translation>
    </message>
    <message>
        <location filename="signatureThresholdTab.py" line="131"/>
        <source>Reset thresholds</source>
        <translation type="unfinished">Zresetuj progi</translation>
    </message>
    <message>
        <location filename="signatureThresholdTab.py" line="131"/>
        <source>Are you sure you want to reset thresholds?</source>
        <translation type="unfinished">Czy na pewno zresetować progi?</translation>
    </message>
    <message>
        <location filename="roidock.py" line="398"/>
        <source>Save shapefile</source>
        <translation type="unfinished">Zapisz shapefile</translation>
    </message>
    <message>
        <location filename="roidock.py" line="868"/>
        <source>Add required fds</source>
        <translation>Dodaj wymagane pola</translation>
    </message>
    <message>
        <location filename="roidock.py" line="868"/>
        <source>It appears that the shapefile </source>
        <translation type="unfinished">Wygląda na to, że ten shapefile</translation>
    </message>
    <message>
        <location filename="roidock.py" line="868"/>
        <source> is missing some fields that are required for the signature calculation. 
Do you want to add the required fields to this shapefile?</source>
        <translation type="unfinished"> nie posiada pól, które są potrzebne do wyznaczania sygnatur. 
Chcesz dodać wymagane pola do tego shapefile?</translation>
    </message>
    <message>
        <location filename="roidock.py" line="1015"/>
        <source>Delete ROIs</source>
        <translation type="unfinished">Usuń OT</translation>
    </message>
    <message>
        <location filename="roidock.py" line="1015"/>
        <source>Are you sure you want to delete highlighted ROIs?</source>
        <translation type="unfinished">Czy na pewno usunąć wybrane OT?</translation>
    </message>
    <message>
        <location filename="roidock.py" line="1308"/>
        <source>Undo save ROI</source>
        <translation type="unfinished">Cofnij zapisywanie OT</translation>
    </message>
    <message>
        <location filename="roidock.py" line="1308"/>
        <source>Are you sure you want to delete the last saved ROI?</source>
        <translation type="unfinished">Czy na pewno usunąć ostatnio zapisany OT?</translation>
    </message>
    <message>
        <location filename="spectralsignatureplot.py" line="218"/>
        <source>Values</source>
        <translation type="unfinished">Wartości</translation>
    </message>
</context>
</TS>
